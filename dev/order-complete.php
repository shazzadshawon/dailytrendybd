<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Order Complete || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Order Complete</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>Order Complete</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- order-complete content section start -->
		<section class="pages checkout order-complete section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="complete-title">
							<p>thank you. your order has been received.</p>
						</div>
						<div class="order-no">
							<ul>
								<li>order no <span>m 2653257</span></li>
								<li>Date<span>may 15, 2016</span></li>
								<li>total<span>&#2547; 186.00</span></li>
								<li>payment method<span>check payment</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row section-padding-top">
					<div class="col-sm-4 col-md-4">
						<div class="log-title">
							<h3><strong>your order</strong></h3>
							<hr />
						</div>
						<div class="place-text clearfix">
							<table>
								<thead>
									<tr>
										<th>product</th>
										<td>total</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>men shoes x 01 <span>Color : Red & Size : 9</span></th>
										<td>&#2547;56.00</td>
									</tr>
									<tr>
										<th>women shoes x 02<span>Color : Red & Size : 8</span></th>
										<td>&#2547;120.00</td>
									</tr>
									<tr>
										<th>shipping  & handling</th>
										<td>&#2547;10.00</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>sub total</th>
										<td>&#2547;186.00</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div class="col-sm-4 col-md-offset-1 col-md-3">
						<div class="order-address bill">
							<div class="log-title">
								<h3><strong>billing address</strong></h3>
								<hr />
							</div>
							<p><span>Crezy fashion</span>, Road no # 45, block B/2, House no # 12/120, Bonoshree, Dhaka Bangladesh.</p>
						</div>
						<div class="order-details">
							<div class="log-title">
								<h3><strong>customer details</strong></h3>
								<hr />
							</div>
							<div class="por-dse">
								<ul>
									<li><span>Name</span><strong>:</strong> MD: Rakib Hossain</li>
									<li><span>Email</span><strong>:</strong> info@domainname.com</li>
									<li><span>Telephone</span><strong>:</strong> (+202) 987 6641 056</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-offset-1 col-md-3">
						<div class="order-address">
							<div class="log-title">
								<h3><strong>shipping address</strong></h3>
								<hr />
							</div>
							<p><span>Crezy fashion</span>, Road no # 45, block B, House no # 12/120, Bonoshree, Dhaka Bangladesh.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- order-complete content section end -->
        <!-- footer section start -->
        <?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
        <?php require('tail.php'); ?>
    </body>
</html>
