<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Product Details || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Product Details</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span><a href="shop.php">Shop</a></li>
								<li><span>/</span>Product Details</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
        <!-- product-details-section-start -->
		<div class="quick-view product-details section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-12 col-sm-5">
								<div class="quick-image">
									<div class="single-quick-image tab-content text-center">
										<div class="simpleLens-container tab-pane  fade in" id="sin-1">
											<a class="simpleLens-image" data-lens-image="img/products/daily2.jpg" href="#"><img src="img/products/daily2.jpg" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade active in" id="sin-2">
											<a class="simpleLens-image" data-lens-image="img/products/daily2.jpg" href="#"><img src="img/products/daily2.jpg" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-3">
											<a class="simpleLens-image" data-lens-image="img/products/daily2.jpg" href="#"><img src="img/products/daily2.jpg" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-4">
											<a class="simpleLens-image" data-lens-image="img/products/daily2.jpg" href="#"><img src="img/products/daily2.jpg" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-5">
											<a class="simpleLens-image" data-lens-image="img/products/daily2.jpg" href="#"><img src="img/products/daily2.jpg" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-6">
											<a class="simpleLens-image" data-lens-image="img/products/daily2.jpg" href="#"><img src="img/products/daily2.jpg" alt="" class="simpleLens-big-image"></a>
										</div>
									</div>
									<div class="quick-thumb">
										<div class="nav nav-tabs">
											<ul id="tabs-details" class="owl-carousel product-slider owl-theme">
												<li class="tablist"><a data-toggle="tab" href="#sin-1"> <img src="img/products/daily2.jpg" alt="quick view" /> </a></li>
												<li class="tablist active"><a  class="active" data-toggle="tab" href="#sin-2"> <img src="img/products/daily2.jpg" alt="quick view" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-3"> <img src="img/products/daily2.jpg" alt="small image" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-4"> <img src="img/products/daily2.jpg" alt="small image" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-5"> <img src="img/products/daily2.jpg" alt="small image" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-6"> <img src="img/products/daily2.jpg" alt="small image" /> </a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-7">
								<div class="quick-right">
									<div class="quick-right-text">
										<h3><strong>product name title</strong></h3>
										<div class="amount">
											<h4>&#2547;65.00</h4>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beenin the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
										<div class="row m-p-b">
											<div class="col-sm-12 col-md-6">
												<div class="por-dse">
													<ul>
														<li><span>Availability</span><strong>:</strong> In stock</li>
														<li><span>Condition</span><strong>:</strong> New product</li>
														<li><span>Category</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
													</ul>
												</div>
											</div>
											<div class="col-sm-12 col-md-6">
												<div class="por-dse color">
													<ul>
														<li><span>size</span><strong>:</strong>  <a href="#">SL</a> <a href="#">SX</a> <a href="#">M</a> <a href="#">XL</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="dse-btn">
											<div class="row">
												<div class="col-sm-7 col-md-6">
													<div class="por-dse">
														<ul>
															<li class="share-btn qty clearfix"><span>quantity</span>
																<form action="#" method="POST">
																	<div class="plus-minus">
																		<a class="dec qtybutton">-</a>
																		<input type="text" value="02" name="qtybutton" class="plus-minus-box">
																		<a class="inc qtybutton">+</a>
																	</div>
																</form>
															</li>
															<li class="share-btn clearfix"><span>share</span>
																<a href="#"><i class="fa fa-facebook"></i></a>
																<a href="#"><i class="fa fa-twitter"></i></a>
																<a href="#"><i class="fa fa-google-plus"></i></a>
																<a href="#"><i class="fa fa-linkedin"></i></a>
																<a href="#"><i class="fa fa-instagram"></i></a>
															</li>
														</ul>
													</div>
												</div>
												<div class="col-sm-5 col-md-6">
													<div class="por-dse clearfix responsive-othre">
														<ul class="other-btn">
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
															<li><a href="#"><i class="fa fa-refresh"></i></a></li>
															<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
														</ul>
													</div>
													<div class="por-dse add-to">
														<a href="#">add to cart</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- single-product item end -->
				</div>

			</div>
		</div>
		<!-- product-details section end -->

        <!-- new-products section start -->
		<section class="new-products single-products section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="section-title">
							<h3>related products</h3>
						</div>
					</div>
				</div>
				<div class="row text-center">
					<div id="new-products" class="owl-carousel product-slider owl-theme">
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<div class="pro-type">
										<span>sale</span>
									</div>
									<a href="#"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>&#2547;52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<div class="pro-type">
										<span>new</span>
									</div>
									<a href="#"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>&#2547;52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>&#2547;52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>&#2547;52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<div class="pro-type">
										<span>new</span>
									</div>
									<a href="#"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>&#2547;52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
					</div>
				</div>
			</div>
		</section>
		<!-- new-products section end -->
        <!--START QUICK VIEW MODAL-->
        <?php require('quick_view.php'); ?>
        <!--END QUICK VIEW MODAL-->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
