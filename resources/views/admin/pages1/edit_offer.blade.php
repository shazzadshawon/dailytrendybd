@extends('layouts.backend')
@section('content')
<div class="row ">
    <div class="col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Offer</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
             
        
        <div class="box-content">
            <div class="box-content">
             	{!! Form::model($pazzle, ['route' => ['offer.update',$pazzle->id], 'method' => 'PUT', 'files'=>true,'name'=>'edit_slider_image','class'=>'form-horizontal']) !!}
                 <fieldset>
                <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Select Category</label>
                        <div class="col-md-10">
                         <select name="pazzle" class="form-control">
                               
                                  @php
                                    $mainCategory = DB::table('categories')
                                    ->where('publication_status',1)
                                    ->get();
                                @endphp
                                @foreach ($mainCategory as $mainCategoryInfo)
                            
                              
                                   <option value="{{ $mainCategoryInfo->category_id }}">{{ $mainCategoryInfo->category_name }}</option>

                                  @endforeach
                                  
                            </select>
                        </div>
                    </div>

               {{--  <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Offer URL</label>
                        <div class="col-md-10">
                            <input type="text"  name="heading" value="{{ $pazzle->heading }}" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4">
                           
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Select Images</label>
                        <div class="col-md-10">
                          
                            <input type="file"  name="pazzle_image" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4" >
<!--                            <input type="file"  name="slider_image[]" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>-->
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-2" for="date01"></label>
                       <div class="col-md-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn">Cancel</button>
                       </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_slider_image'].elements['pazzle'].value = '{{$pazzle->pazzle}}'; 
    document.forms['edit_slider_image'].elements['publication_status'].value = '{{$pazzle->publication_status}}';    
</script>
@endsection

