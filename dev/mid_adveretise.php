<section class="summer summer-one section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div class="left-banner">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-10 col-md-offset-5 col-md-7">
                            <div class="count-text clearfix">
                                <ul id="countdown-2">
                                    <li id="countdown_back" style="display: none"><?php echo "10 oct 2017 01:00:00";?></li>
                                    <li>
                                        <span class="days timenumbers"></span>
                                        <p class="timeRefDays timedescription">days</p>
                                    </li>
                                    <li>
                                        <span class="hours timenumbers"></span>
                                        <p class="timeRefHours timedescription">hrs</p>
                                    </li>
                                    <li>
                                        <span class="minutes timenumbers"></span>
                                        <p class="timeRefMinutes timedescription">mins</p>
                                    </li>
                                    <li>
                                        <span class="seconds timenumbers"></span>
                                        <p class="timeRefSeconds timedescription">secs</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="bannertext">
                                <h4><a href="#">summer collection</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                <a href="shop.php">Shop now <img src="img/link-angle.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="right-banner">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-4">
                            <span>&#2547;65</span>
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-8 text-right">
                            <div class="bannertext">
                                <h4><a href="#">winter collection</a></h4>
                                <p>Lorem ipsum dolor sit amet, consecte adipisicing elit, sed do eiusmod tempor incididunt ut enim.</p>
                                <a href="shop.php">Shop now  <img src="img/link-angle.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>