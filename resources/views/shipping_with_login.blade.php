@extends('layouts.frontend')

@section('content')
    <!-- HEADER -->

    {!! Form::open(['route' => 'Order.store', 'method' => 'post']) !!}




    <div class="pages-title section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="pages-title-text">
                        <h3>Checkout</h3>
                        <ul>
                            <li><a href="{{url('/')}}p">Home</a></li>
                            <li><span>/</span>Checkout</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <section class="pages checkout login-page section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="login-text billing new-customer">
                        <div class="log-title">
                            <h3><strong>billing details</strong></h3>
                            <hr />
                        </div>
                        <div class="custom-input">
                            <label for="fname">Name</label>
                            <input type="text" name="name" value="{{$customer->customer_name}}" />

                            <label for="fname">Email Address</label>
                            <input type="text" name="email" value="{{$customer->email_address}}" />

                            <label for="fname">Phone Number</label>
                            <input type="text" name="phone" value="{{$customer->phone_number}}" />

                            <label for="fname">Address</label>
                            <div class="review-mess">
                                <textarea rows="4" placeholder="Address" name="address">
                                    <?php print_r($customer->address); ?>
                                </textarea>
                            </div>

                            {{--<div class="review-mess">--}}
                                {{--<textarea rows="4" placeholder="Order notes here" name="notes"></textarea>--}}
                            {{--</div>--}}

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="log-title">
                        <h3><strong>your order</strong></h3>
                        <hr />
                    </div>
                    <div class="place-text clearfix">
                        <table>
                            <thead>
                            <tr>
                                <th>product</th>
                                <td>total</td>
                            </tr>
                            </thead>
                            <tbody>

                            @php

                                $i=1;
                                $j=0;
                                $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get();

                            @endphp


                            @foreach ($addTocart as $cart)

                                @php
                                    $addTocartImage = DB::table('product_images')->where('product_id', $cart->product_id)->first();
                                    $sub_total=$cart->product_price*$cart->product_quantity;

                                $product = DB::table('products')->where('publication_status','!=',2)->where('id',$cart->product_id)->first();
                                @endphp



                                <tr>
                                    <th>{{$product->product_name}} x {{$cart->product_quantity}}  <span> Size : @if($cart->size==0) N/A @else {{$cart->size}} @endif</span><span> Code : {{$product->product_code}} </span></th>
                                    <td>&#2547; {{$sub_total}}
                                        <a href="{{URL::to('/remove-cart-product/'.$cart->id)}}"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                @php
                                    $j = $j+ $sub_total;
                                    $i++;
                                @endphp

                            @endforeach


                            </tbody>
                            <tfoot>
                            <tr>
                                <th>total</th>
                                <td>&#2547;{{$j}}</td>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="submit-text place-order floatright">

                            <button type="submit"> Place Order</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

















    {!! Form::close() !!}
@endsection