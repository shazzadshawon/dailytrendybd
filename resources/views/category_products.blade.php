@extends('layouts.frontend')

@section('content')

    <div class="pages-title section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="pages-title-text">
                        <h3>{{ $subCategory->sub_category_name }}</h3>
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><span>/</span>{{ $subCategory->sub_category_name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <section class="pages products-page section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 text-center">
                    <div class="sidebar left-sidebar">

                        <div class="s-side-text">
                            <div class="sidebar-title">
                                <?php
                                $subcategories = DB::table('sub_categories')->orderBy('sub_category_id','asc')->get();
                                ?>
                                <h4>CATEGORY</h4>
                            </div>
                            <div class="facturer category text-bg">
                                <ul>
                                    @foreach($subcategories as $sub)
                                    <li><a href="{{url('Category-products/'.$sub->sub_category_id)}}">{{$sub->sub_category_name}} <span class="floatright"></span></a></li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="right-products">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section-title clearfix ">
                                    <ul>
                                        <li class="pull-right">
                                            <ul class="nav nav-tabs ">
                                                <li class="active"><a data-toggle="tab" href="#grid"> <img src="{{asset('public/frontend/img/products/grid-icon.png')}}" alt="grid-icon" /> </a></li>
                                                <li><a data-toggle="tab" href="#list"> <img src="{{asset('public/frontend/img/products/list-icon.png')}}" alt="list-icon" /> </a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>

                        @include('includes.shop_page_products')

                        <!--							<div class="row">-->
                        <!--								<div class="col-xs-12">-->
                        <!--									<div class="blog-pagination">-->
                        <!--										<nav>-->
                        <!--											<ul class="pagination">-->
                        <!--												<li>-->
                        <!--													<a href="#" aria-label="Previous">-->
                        <!--														<span aria-hidden="true">prev</span>-->
                        <!--													</a>-->
                        <!--												</li>-->
                        <!--												<li class="active"><a href="#">1</a></li>-->
                        <!--												<li><a href="#">2</a></li>-->
                        <!--												<li><a href="#">3</a></li>-->
                        <!--												<li><a href="#">4</a></li>-->
                        <!--												<li>-->
                        <!--													<a href="#" aria-label="Next">-->
                        <!--														<span aria-hidden="true">next</span>-->
                        <!--													</a>-->
                        <!--												</li>-->
                        <!--											</ul>-->
                        <!--										</nav>-->
                        <!--									</div>-->
                        <!--								</div>-->
                        <!--							</div>-->


                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection