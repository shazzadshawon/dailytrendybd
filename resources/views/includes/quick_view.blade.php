<!-- quick view start -->
<div class="quick-view modal fade in" id="quick-view">
    <div class="container">
        <div class="row">
            <div id="view-gallery" class="owl-carousel product-slider owl-theme">

                    @include('includes.quick_view_slide')
                <!-- single-product item end -->

                    @include('includes.quick_view_slide')
                <!-- single-product item end -->
            </div>
        </div>
    </div>
</div>
<!-- quick view end -->