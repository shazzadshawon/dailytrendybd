@extends('layouts.frontend')

@section('content')

    <div class="pages-title section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="pages-title-text">
                        <h3>Cart</h3>
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><span>/</span>Cart</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <section class="pages cart-page section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="cart-table text-center">
                            <thead>
                            <tr>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                                <th>Remove</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                            $total=0;
                            @endphp
                            @foreach($carts as $cart)
                                @php
                                    //$ct = DB::table('categories')->where('category_id',$productInfo->category_id)->first();
                                    $image = DB::table('product_images')->where('product_id',$cart->product_id)->orderBy('product_image_id','desc')->first();
                                @endphp
                                <tr>
                                    <td class="td-img text-left">
                                        <a href="#"><img src="{{asset('product_image/'.$image->product_image)}}" alt="Add Product" style="width: 72px; height: 108px;" /></a>
                                        <div class="items-dsc">
                                            <h5><a href="#">{{$cart->product_name}}</a></h5>

                                            <p class="itemcolor">Size   : <span>{{$cart->size}}</span></p>
                                        </div>
                                    </td>
                                    <td>&#2547; {{$cart->product_price}}</td>
                                    <td>
                                        {{$cart->product_quantity}}
                                    </td>
                                    <td>
                                        <div class="total-price">
                                            @php
                                                $price = $cart->product_price * $cart->product_quantity;
                                                $total=$total + $price;
                                            @endphp
                                            &#2547; {{$price}}
                                        </div>
                                    </td>
                                    <td>
                                        <a href="{{url('remove-cart-product/'.$cart->id)}}"><i class="pe-7s-close" title="Remove this product"></i></a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row margin-top">

                <div class="col-sm-6">
                    <div class="single-cart-form">
                        <div class="cart-form-title">
                            <h4>payment details</h4>
                        </div>
                        <div class="cart-form-text">
                            <table>
                                <tbody>
                                <tr>
                                    <th>subtotal</th>
                                    <td>&#2547; {{$total}}</td>
                                </tr>
                                {{--<tr>--}}
                                    {{--<th>shipping</th>--}}
                                    {{--<td>&#2547;10.00</td>--}}
                                {{--</tr>--}}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class="tfoot-padd">grand total</th>
                                    <td class="tfoot-padd">&#2547; {{$total}}</td>
                                </tr>
                                </tfoot>
                            </table>
                            <div class="submit-text">
                                <a href="{{url('shipping')}}">procced to checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection