<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="left-header clearfix">
                        <ul>
                            <li><p><i class="pe-7s-call"></i>(+880) 1910 000251</p></li>
                            <li><p><i class="pe-7s-clock"></i>Mon-fri : 9:00-19:00</p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="right-header">
                        <ul>
                            <li><i class="pe-7s-search"></i>
                                <form method="get" id="searchform" action="{{url('product_search')}}">
                                    <input type="text" value="" name="query" id="ws" placeholder="Search product..." />
                                    <button type="submit"><i class="pe-7s-search"></i></button>
                                </form>
                            </li>
                            <li><a href="#"><i class="pe-7s-settings"></i></a>
                                <ul class="drop-setting">
                                    @if(Session::has('customer_name'))
                                    <li><a href="{{url('logout')}}"><i class="fa fa-user"></i>
                                           logout <br> ( {{Session::get('customer_name')}} )
                                        </a></li>
                                    @else
                                        <li><a href="{{url('User-Register')}}"><i class="fa fa-lock"></i>Login / Register</a></li>
                                    @endif
                                    <li><a href="{{url('cart')}}"><i class="fa fa-shopping-cart"></i>My cart</a></li>
                                    <li><a href="{{url('wishlist')}}"><i class="fa fa-heart"></i>My wishlist</a></li>
                                    <li><a href="{{url('shipping')}}"><i class="fa">&#2547;</i>Check out</a></li>

                                </ul>
                            </li>
                            <li><a href="{{url('cart')}}"><i class="pe-7s-cart"></i><span class="color1"></span></a>

                                @include('includes.header_cart')
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="" style="padding: 0;">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" style="padding-bottom: 30px; padding-left: 50px; text-align: center">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('message') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('info'))
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('info') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('warning'))
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('warning') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('danger'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('danger') !!}</li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom header-bottom-one" id="sticky-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-md-2">
                    <div class="logo">
                        <a href="{{url('/')}}"><img src="{{asset('public/frontend/img/daily_logo_a.png')}}" alt="Daily Trendy BD" /></a>
                    </div>
                </div>
                <div class="col-sm-10 col-md-10">
                    <?php
                        $main_categories = DB::table('categories')->where('publication_status',1)->get();

                    ?>
                    <div class="mainmenu clearfix">
                        <nav>

                            @include('includes.menu_content')

                        </nav>
                    </div>

                    @include('includes.menu_content_mobile')
                    <!-- mobile menu end -->
                </div>
            </div>
        </div>
    </div>
</header>