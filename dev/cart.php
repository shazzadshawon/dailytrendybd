<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Cart || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Cart</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>Cart</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- cart content section start -->
		<section class="pages cart-page section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="table-responsive">
							<table class="cart-table text-center">
								<thead>
									<tr>
										<th>Items</th>
										<th>Price</th>
										<th>Quantity</th>
										<th>Total Price</th>
										<th>Remove</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="td-img text-left">
											<a href="#"><img src="img/cart/daily2.jpg" alt="Add Product" style="width: 72px; height: 108px;" /></a>
											<div class="items-dsc">
												<h5><a href="#">Product Title demo</a></h5>
												<p class="itemcolor">Color : <span>Blue</span></p>
												<p class="itemcolor">Size   : <span>SL</span></p>
											</div>
										</td>
										<td>&#2547;52.00</td>
										<td>
											<form action="#" method="POST">
												<div class="plus-minus">
													<a class="dec qtybutton">-</a>
													<input type="text" value="02" name="qtybutton" class="plus-minus-box">
													<a class="inc qtybutton">+</a>
												</div>
											</form>
										</td>
										<td>
											<div class="total-price">
												&#2547;104.00
											</div>
										</td>
										<td><i class="pe-7s-close" title="Remove this product"></i></td>
									</tr>
									<tr>
										<td class="td-img text-left">
											<a href="#"><img src="img/cart/daily3.jpg" alt="Add Product" style="width: 72px; height: 108px;" /></a>
											<div class="items-dsc">
												<h5><a href="#">Product Title</a></h5>
												<p class="itemcolor">Color : <span>Blue</span></p>
												<p class="itemcolor">Size   : <span>SL</span></p>
											</div>
										</td>
										<td>&#2547;52.00</td>
										<td>
											<form action="#" method="POST">
												<div class="plus-minus">
													<a class="dec qtybutton">-</a>
													<input type="text" value="02" name="qtybutton" class="plus-minus-box">
													<a class="inc qtybutton">+</a>
												</div>
											</form>
										</td>
										<td>
											<div class="total-price">
												&#2547;104.00
											</div>
										</td>
										<td><i class="pe-7s-close" title="Remove this product"></i></td>
									</tr>
									<tr>
										<td class="td-img text-left">
											<a href="#"><img src="img/cart/daily4.jpg" alt="Add Product" style="width: 72px; height: 108px;" /></a>
											<div class="items-dsc">
												<h5><a href="#">Product Title demo</a></h5>
												<p class="itemcolor">Color : <span>Blue</span></p>
												<p class="itemcolor">Size   : <span>SL</span></p>
											</div>
										</td>
										<td>&#2547;52.00</td>
										<td>
											<form action="#" method="POST">
												<div class="plus-minus">
													<a class="dec qtybutton">-</a>
													<input type="text" value="02" name="qtybutton" class="plus-minus-box">
													<a class="inc qtybutton">+</a>
												</div>
											</form>
										</td>
										<td>
											<div class="total-price">
												&#2547;104.00
											</div>
										</td>
										<td><i class="pe-7s-close" title="Remove this product"></i></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row margin-top">

					<div class="col-sm-6">
						<div class="single-cart-form">
							<div class="cart-form-title">
								<h4>payment details</h4>
							</div>
							<div class="cart-form-text">
								<table>
									<tbody>
										<tr>
											<th>subtotal</th>
											<td>&#2547;360.00</td>
										</tr>
										<tr>
											<th>shipping</th>
											<td>&#2547;10.00</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<th class="tfoot-padd">grand total</th>
											<td class="tfoot-padd">&#2547;370.00</td>
										</tr>
									</tfoot>
								</table>
								<div class="submit-text">
									<a href="checkout.php">procced to checkout</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- cart content section end -->
        <!-- footer section start -->
        <?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
