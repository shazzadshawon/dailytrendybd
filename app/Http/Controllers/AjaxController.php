<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubCategory;
use App\Product;
use DB;
class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SubCategory($id)
    {
        $sub_categories = SubCategory::where('category_id',$id)->get();

        return view('admin.pages.select_sub_categories')->withSubCategories($sub_categories);
    }


        public function autoComplete(Request $request) {

        $query = $request->get('term','');
        
        $products=Product::where('product_name','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->product_name,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

   public function search(Request $request)
    {
        if($request->ajax()){
            $output="";
        $users = DB::table('products')->where('product_name','LIKE','%'.$request->search.'%')->get();
        if($users){
           foreach ($users as $user) {
              // $output.='<a href="my-test-mail/'.$user->id.'">'.$user->name.'</a>'.'<br>';

            $output.='<div class="show" align="left"><a href="http://www.kenakatazone.com/product-details/'.$user->id.'" class="country_name">'.$user->product_name.'</a></div>';

           }
           return Response($output);  
        }
        }
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
