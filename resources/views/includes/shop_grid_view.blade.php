<?php
$x=0;
?>
@foreach($products as $product)
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="single-product">
            <div class="product-img">
                <div class="pro-type">
                    {{--<span>sale</span>--}}
                </div>
                <?php
                $image = DB::table('product_images')->where('product_id',$product->id)->orderBy('product_image_id','desc')->first();
                ?>

                <a href="{{url('product-details/'.$product->id)}}"><img class="featured_product_imgHeight" src="{{asset('product_image/'.$image->product_image)}}" alt="Product Title" /></a>
            </div>
            <div class="product-dsc">
                <p><a href="{{url('product-details/'.$product->id)}}">{{$product->product_name}}</a></p>
                <span>&#2547;@if($product->discount > 0){{ $product->product_price-($product->product_price*$product->discount)/100}}@else{{$product->product_price}}@endif</span>
            </div>
            <div class="actions-btn">

                {!! Form::open(['route' => 'wishlist.store','files'=>true,'id'=>$product->id]) !!}
                <input type="hidden" name="session_id"
                       value="{{Session::getId()}}">
                <input type="hidden" name="product_id"
                       value="{{$product->id}}">


                {!! Form::close() !!}


                <a href="" onclick="document.getElementById({{$product->id}}).submit(); " data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>

                {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'id'=>$x]) !!}

                <input type="hidden" name="product_id" value="{{$product->id}}">
                <input type="hidden" name="product_name" value="{{$product->product_name}}">
                <input type="hidden" name="product_name_bn"
                       value="{{$product->product_name_bn}}">
                <input type="hidden" name="product_code" value="{{$product->product_code}}">
                <input type="hidden" name="product_price"
                       value="@if($product->discount > 0){{ $product->product_price-($product->product_price*$product->discount)/100}}@else{{$product->product_price}}@endif">
                <input type="hidden" name="publication_status"
                       value="{{$product->publication_status}}">

                <input type="hidden" name="product_quantity" value="1">
                <input type="hidden" name="size" value="0">


                {!! Form::close() !!}


                <a href="" onclick="document.getElementById({{$x}}).submit();" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
            </div>
        </div>
    </div>
    <?php
    $x++;
    ?>
@endforeach

{{$products->links()}}
