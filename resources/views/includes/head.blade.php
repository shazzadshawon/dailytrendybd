<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- favicon -->
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

<link rel="apple-touch-icon" href="apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->
<!-- google fonts -->
<link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
<!-- all css here -->
<!-- bootstrap v3.3.6 css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">
<!-- animate css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/animate.css')}}">
<!-- pe-icon-7-stroke -->
<link rel="stylesheet" href="{{asset('public/frontend/css/pe-icon-7-stroke.min.css')}}">
<!-- pe-icon-7-stroke -->
<link rel="stylesheet" href="{{asset('public/frontend/css/jquery.simpleLens.css')}}">
<!-- jquery-ui.min css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/jquery-ui.min.css')}}">
<!-- meanmenu css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/meanmenu.min.css')}}">
<!-- nivo.slider css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/nivo-slider.css')}}">
<!-- owl.carousel css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/owl.carousel.css')}}">
<!-- font-awesome css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/font-awesome.min.css')}}">
<!-- style css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">
<!-- responsive css -->
<link rel="stylesheet" href="{{asset('public/frontend/css/responsive.css')}}">
<!-- modernizr js -->
<script src="{{asset('public/frontend/js/vendor/modernizr-2.8.3.min.js')}}"></script>