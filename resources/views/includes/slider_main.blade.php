<div class="main-slider-one slider-area">
    <div id="wrapper">
        <div class="slider-wrapper">
            <div id="mainSlider" class="nivoSlider" style="max-height: 843px;">
                @foreach($sliders as $slide)
                    <img style="widows: 100%;" src="{{asset('slider_image/'.$slide->slider_image)}}" alt="main slider" title="#{{$slide->slider_image_id}}"/>
                @endforeach

            </div>
            @foreach($sliders as $slide)
            <div id="{{$slide->slider_image_id}}" class="nivo-html-caption slider-caption">
                <div class="container">
                    <div class="slider-left">
                        <div class="text-img animated bounceInLeft">
                            <h1>{{$slide->title}}</h1> <hr>
                            <h4>{{$slide->subtitle}}</h4>
                        </div>
                        <div class="animated slider-btn fadeInUpBig">
                            <a class="shop-btn" href="">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>