@foreach($products as $product)
<div class="single-product">
    <div class="row">
        <?php
        $image = DB::table('product_images')->where('product_id',$product->id)->orderBy('product_image_id','desc')->first();
        ?>
        <div class="col-md-4">
            <div class="product-img">
                <a href="{{url('product-details/'.$product->id)}}"><img src="{{asset('product_image/'.$image->product_image)}}" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">
                <h4><a href="{{url('product-details/'.$product->id)}}">{{$product->product_name}}</a></h4>
                <h3>&#2547;52.00</h3>
                <p><?php print_r($product->description  );?></p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    {{--<li><a href="#">06 Reviews</a></li>--}}
                    {{--<li><a href="#">add your review</a></li>--}}
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix pull-right">
                    <li><a class="" href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a class="" href=""><i class="pe-7s-like"></i></a></li>

                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="{{url('product-details/'.$product->id)}}"><i class="pe-7s-cart"></i>View Details</a>
            </div>
        </div>
    </div>
</div>
@endforeach
{{$products->links()}}