

<footer id="footer">
     <div class="container">
            <!-- introduce-box -->
            <div id="introduce-box" class="row">
                <div class="col-md-3">
                    <div id="address-box">
                        <a href="{{ url('/') }}"><img src="{{ asset('assets/images/lakesLogo_new.png') }}" alt="" /></a>
                        <div id="address-list">
                            <div class="tit-name">Address:</div>
                            <div class="tit-contain">MRC Bhaban,Mirpur-1, Mirpur, Dhaka(1216), Bangladesh.</div>
                            <div class="tit-name">Phone:</div>
                            <div class="tit-contain">+8801611392222</div>
                            <div class="tit-name">Email:</div>
                            <div class="tit-contain">lakespoint@gmail.com</div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="introduce-title">Company</div>
                            <ul id="introduce-company"  class="introduce-list">
                                <li><a href="{{ url('about-us') }}">About Us</a></li>
                                <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                                <li><a href="{{ url('delivery_policy') }}">Delivery Information</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <div class="introduce-title">My Account</div>
                            <ul id = "introduce-Account" class="introduce-list">
                               
                                <li><a href="{{ url('shipping') }}">My Order</a></li>
                                <li><a href="{{ url('view-wishlist') }}">My Wishlist</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div id="contact-box">
                       {{--  <div class="introduce-title">Newsletter</div>
                        <div class="input-group" id="mail-box">
                          <input type="text" placeholder="Your Email Address"/>
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button">OK</button>
                          </span>
                        </div><!-- /input-group --> --}}
                        <div class="introduce-title">Let's Socialize</div>
                        <div class="social-link">
                            <a href="https://www.facebook.com/ShoppingLake/"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            <a href="#"><i class="fa fa-vk"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                    
                </div>
            </div><!-- /#introduce-box -->
        
            <!-- #trademark-box -->
            
            @include('frontend/footer_treadmark')

            <div>
                <p class="text-center"> <h2 align="center"> Made With &nbsp; <i class="fa fa-heart" aria-hidden="true"></i> &nbsp; By &nbsp; <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a> </h2 align="center"> </p>
            </div><!-- /#footer-menu-box -->
     </div>
</footer>










