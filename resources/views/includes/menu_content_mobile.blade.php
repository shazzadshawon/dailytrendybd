<div class="mobile-menu-area">
    <div class="mobile-menu">
        <nav id="dropdown">
            <ul>
                <li><a href="{{url('/')}}">Home</a>

                </li>
                <li><a href="">Shop</a>
                    <ul>
                        @foreach($main_categories as $cat)
                        <li><a href="#">{{$cat->category_name}}</a>
                            <ul>
                                 @php
                            $sub_categories = DB::table('sub_categories')->where('category_id',$cat->category_id)->get();
                        @endphp

                        @foreach($sub_categories as $sub)
                                <li>
                                    
                                    <a href="{{url('Category-products/'.$sub->sub_category_id)}}">{{$sub->sub_category_name}}</a>
                                </li>
                        @endforeach
                            </ul>
                        </li>
                    @endforeach
                    </ul>
                </li>
                <li><a href="{{url('about')}}">About</a></li>
                <li><a href="{{url('contact')}}">Contact</a></li>
            </ul>
        </nav>
    </div>
</div>