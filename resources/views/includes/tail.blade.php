<script src="{{asset('public/frontend/js/vendor/jquery-1.12.0.min.js')}}"></script>
<!-- bootstrap js -->
<script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>
<!-- owl.carousel js -->
<script src="{{asset('public/frontend/js/owl.carousel.min.js')}}"></script>
<!-- meanmenu js -->
<script src="{{asset('public/frontend/js/jquery.meanmenu.js')}}"></script>
<!-- countdown JS -->
<script src="{{asset('public/frontend/js/countdown.js')}}"></script>
<!-- nivo.slider JS -->
<script src="{{asset('public/frontend/js/jquery.nivo.slider.pack.js')}}"></script>
<!-- simpleLens JS -->
<script src="{{asset('public/frontend/js/jquery.simpleLens.min.js')}}"></script>
<!-- jquery-ui js -->
<script src="{{asset('public/frontend/js/jquery-ui.min.js')}}"></script>
<!-- sticky js -->
<script src="{{asset('public/frontend/js/sticky.js')}}"></script>
<!-- plugins js -->
<script src="{{asset('public/frontend/js/plugins.js')}}"></script>
<!-- main js -->
<script src="{{asset('public/frontend/js/main.js')}}"></script>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyDElOclVeevpAwrC01r05nJSI6Bri8dSXA"></script>

<script src="{{asset('public/frontend/js/map-script.js')}}"></script>