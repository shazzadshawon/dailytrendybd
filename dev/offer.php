<section class="collection-area section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title" style="text-align: center">
                    <h3>offers</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="blog" class="owl-carousel product-slider owl-theme">
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href="product-detail.php"><img class="offer_img_height" src="img/offers/daily2.jpg" alt=""/></a>
                            </div>
                        </div>
                        <div class="blog-dsc">
                            <p><a href="product-detail.php">offer title</a></p>
                            <p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>
                        </div>
                    </div>
                </div>
                <!-- single blog item end -->
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href="product-detail.php"><img class="offer_img_height" src="img/offers/daily3.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="blog-dsc">
                            <p><a href="product-detail.php">offer title</a></p>
                            <p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>
                        </div>
                    </div>
                </div>
                <!-- single blog item end -->
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href="product-detail.php"><img class="offer_img_height" src="img/offers/daily4.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="blog-dsc">
                            <p><a href="product-detail.php">offer title</a></p>
                            <p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>
                        </div>
                    </div>
                </div>
                <!-- single blog item end -->
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href="product-detail.php"><img class="offer_img_height" src="img/offers/daily2.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="blog-dsc">
                            <p><a href="product-detail.php">offer title</a></p>
                            <p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>
                        </div>
                    </div>
                </div>
                <!-- single blog item end -->
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href="product-detail.php"><img class="offer_img_height" src="img/offers/daily3.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="blog-dsc">
                            <p><a href="product-detail.php">offer title</a></p>
                            <p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>
                        </div>
                    </div>
                </div>
                <!-- single blog item end -->
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href="product-detail.php"><img class="offer_img_height" src="img/offers/daily4.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="blog-dsc">
                            <p><a href="product-detail.php">offer title</a></p>
                            <p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>
                        </div>
                    </div>
                </div>
                <!-- single blog item end -->
            </div>
        </div>
    </div>
</section>