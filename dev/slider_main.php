<div class="main-slider-one slider-area">
    <div id="wrapper">
        <div class="slider-wrapper">
            <div id="mainSlider" class="nivoSlider" style="max-height: 843px;">
                <img src="img/slider/home1/some2.jpg" alt="main slider" title="#htmlcaption"/>
                <img src="img/slider/home1/some.jpg" alt="main slider" title="#htmlcaption2" />
                <img src="img/slider/home1/some3.jpg" alt="main slider" title="#htmlcaption3"/>
                <img src="img/slider/home1/some2.jpg" alt="main slider" title="#htmlcaption4"/>
                <img src="img/slider/home1/some.jpg" alt="main slider" title="#htmlcaption5"/>
            </div>
            <div id="htmlcaption" class="nivo-html-caption slider-caption">
                <div class="container">
                    <div class="slider-left">
                        <div class="text-img animated bounceInLeft">
                            <h1>This is slider header</h1> <hr>
                            <h4>slider sub-header</h4>
                        </div>
                        <div class="animated slider-btn fadeInUpBig">
                            <a class="shop-btn" href="shop.php">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="htmlcaption2" class="nivo-html-caption slider-caption">
                <div class="container">
                    <div class="slider-left">
                        <div class="text-img animated bounceInLeft">
                            <h1>This is slider header</h1> <hr>
                            <h4>slider sub-header</h4>
                        </div>
                        <div class="animated slider-btn fadeInUpBig">
                            <a class="shop-btn" href="shop.php">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="htmlcaption3" class="nivo-html-caption slider-caption">
                <div class="container">
                    <div class="slider-left">
                        <div class="text-img animated bounceInLeft">
                            <h1>This is slider header</h1> <hr>
                            <h4>slider sub-header</h4>
                        </div>
                        <div class="animated slider-btn fadeInUpBig">
                            <a class="shop-btn" href="shop.php">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="htmlcaption4" class="nivo-html-caption slider-caption">
                <div class="container">
                    <div class="slider-left">
                        <div class="text-img animated bounceInLeft">
                            <h1>This is slider header</h1> <hr>
                            <h4>slider sub-header</h4>
                        </div>
                        <div class="animated slider-btn fadeInUpBig">
                            <a class="shop-btn" href="shop.php">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="htmlcaption5" class="nivo-html-caption slider-caption">
                <div class="container">
                    <div class="slider-left">
                        <div class="text-img animated bounceInLeft">
                            <h1>This is slider header</h1> <hr>
                            <h4>slider sub-header</h4>
                        </div>
                        <div class="animated slider-btn bounceInLeft">
                            <a class="shop-btn" href="shop.php">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>