<!doctype html>
<html class="no-js" lang="">
<head>
    <title>Daily Treandy BD</title>
    @include('includes.head')
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- header section start -->

@include('includes.header')


@yield('content')

@include('includes.footer')

@include('includes.tail')
</body>
</html>
