<ul class="drop-cart">
    <li>
        <a href="cart.php"><img src="img/cart/1.png" alt="" /></a>
        <div class="add-cart-text">
            <p><a href="#">White Shirt</a></p>
            <p>&#2547;50.00</p>
            <span>Color : Blue</span>
            <span>Size  : SL</span>
        </div>
        <div class="pro-close">
            <i class="pe-7s-close"></i>
        </div>
    </li>
    <li>
        <a href="cart.php"><img src="img/cart/2.png" alt="" /></a>
        <div class="add-cart-text">
            <p><a href="#">White Shirt</a></p>
            <p>&#2547;50.00 x 2</p>
            <span>Color : Blue</span>
            <span>Size   : SL</span>
        </div>
        <div class="pro-close">
            <i class="pe-7s-close"></i>
        </div>
    </li>
    <li class="total-amount clearfix">
        <span class="floatleft">total</span>
        <span class="floatright"><strong>= &#2547;150.00</strong></span>
    </li>
    <li>
        <div class="goto text-center">
            <a href="cart.php"><strong>go to cart &nbsp;<i class="pe-7s-angle-right"></i></strong></a>
        </div>
    </li>
    <li class="checkout-btn text-center">
        <a href="checkout.php">Check out</a>
    </li>
</ul>