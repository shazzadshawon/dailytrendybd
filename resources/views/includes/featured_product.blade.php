<section class="featured-products single-products section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>FEATURED PRODUCTS</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="product-tab nav nav-tabs">
                    <ul>
                        <li style="margin-bottom: 30px" class="active"><a data-toggle="tab" href="#all">all shop</a></li>
                        @foreach($main_categories as $main)
                            <li style="margin-bottom: 30px"><a data-toggle="tab" href="#tab-{{$main->category_id}}">{{$main->category_name}}</a></li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
        <div class="row text-center tab-content">
            <!--First tab pane here-->
            <div class="tab-pane  fade in active" id="all">
                <div id="tab-carousel-1" class="owl-carousel product-slider owl-theme">
                    <?php
                    $productsAll = DB::table('products')->where('offer_status',1)->orderby('id','desc')->take(5)->get();
                    $x=0;
                    ?>
                    @foreach($productsAll as $pro)
                        <?php
                        $image = DB::table('product_images')->where('product_id',$pro->id)->first();
                        ?>
                        <div class="col-xs-12">
                            <div class="single-product">
                                <div class="product-img">
                                    <div class="pro-type">
                                        <span>Featured</span>
                                    </div>
                                    @if(!empty($image))
                                        <a href="{{url('product-details/'.$pro->id)}}"><img  style="height: 300px;"  src="{{asset('product_image/'.$image->product_image)}}" alt="Product Title" /></a>
                                    @endif
                                </div>
                                <div class="product-dsc">
                                    <p><a href="{{url('product-details/'.$pro->id)}}">{{$pro->product_name}}</a></p>
                                    <span>$ @if($pro->discount > 0){{ $pro->product_price-($pro->product_price*$pro->discount)/100}}@else{{$pro->product_price}}@endif</span>
                                </div>

                            </div>


                        </div>
                        <?php
                        $x++;
                        ?>
                @endforeach
                <!-- single product end -->

                </div>
            </div>
           
           
           
           
           
            <?php
                $z=2;
                $x=0;
            ?>
            @foreach($main_categories as $main)
                <?php
                $products = DB::table('products')->where('offer_status',1)->orderby('id','desc')->take(5)->where('category_id',$main->category_id)->get();
                ?>
            <div class="tab-pane  fade in" id="tab-{{$main->category_id}}">
                <div id="tab-carousel-{{$z}}" class="owl-carousel product-slider owl-theme">
                    @foreach($products as $pro)
                        <?php
                        $image = DB::table('product_images')->where('product_id',$pro->id)->first();
                        ?>
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <div class="pro-type">
                                    <span>Featured</span>
                                </div>
                                @if(!empty($image))
                                    <a href="{{url('product-details/'.$pro->id)}}"><img  style="height: 300px;"  src="{{asset('product_image/'.$image->product_image)}}" alt="Product Title" /></a>
                                @endif
                            </div>
                            <div class="product-dsc">
                                <p><a href="{{url('product-details/'.$pro->id)}}">{{$pro->product_name}}</a></p>
                                <span>$ @if($pro->discount > 0){{ $pro->product_price-($pro->product_price*$pro->discount)/100}}@else{{$pro->product_price}}@endif</span>
                            </div>

                        </div>
                    </div>
                        <?php

                        $x++;
                        ?>
                    @endforeach
                    <!-- single product end -->

                </div>
            </div>
                    <?php
                    $z++;
                    ?>
            @endforeach
        </div>
    </div>
</section>