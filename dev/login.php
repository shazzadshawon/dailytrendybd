<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Login || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Login</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>Login</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- login content section start -->
		<section class="pages login-page section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="login-text">
							<div class="log-title">
								<h3><strong>registered customers</strong></h3>
								<hr />
							</div>
							<div class="custom-input">
								<p>If you have an account with us, Please log in!</p>
								<form action="mail.php" method="post">
									<input type="text" name="email" placeholder="Email" />
									<input type="password" name="password" placeholder="Password" />
									<a class="forget" href="#">Forget your password?</a>
									<div class="submit-text coupon">
										<a href="my-account.php">login</a>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="login-text new-customer">
							<div class="log-title">
								<h3><strong>new customers</strong></h3>
								<hr />
							</div>
							<div class="custom-input">
								<form action="mail.php" method="post">
									<input type="text" name="name" placeholder="Name" />
									<input type="text" name="email" placeholder="Email Address" />
									<input type="text" name="number" placeholder="Phone Number" />
									<div class="custom-select">
										<select class="form-control">
											<option> Country</option>
											<option> Bangladesh </option>
											<option> United States </option>
											<option> United Kingdom </option>
											<option> Canada </option>
											<option> Malaysia </option>
											<option> United Arab Emirates </option>
										</select>
									</div>
									<div class="custom-select">
										<select class="form-control">
											<option> Town / City</option>
											<option> Aberdeen </option>
											<option> Bedfordshire </option>
											<option> Caerphilly </option>
											<option> Denbighshire </option>
											<option> East Ayrshire </option>
											<option> Falkirk </option>
										</select>
									</div>
									<input type="text" name="address" placeholder="Address" />
									<input type="text" name="address" placeholder="Password" />
									<input type="password" name="password" placeholder="Confirm Password" />
									<label class="first-child">
										<input type="radio" name="rememberme" value="forever">
										Sign up for our newsletter!
									</label>
									<label>
										<input type="radio" name="rememberme" value="forever">
										Receive special offers from our partners!
									</label>
									<div class="submit-text coupon">
										<a href="my-account.php">Register</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- login content section end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
