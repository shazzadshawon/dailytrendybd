<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Contact || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Contact</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>Contact</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- contact section start -->
		<section class="pages checkout contact login-page section-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-1 col-sm-5">
						<div class="get-touch">
							<div class="log-title">
								<h3><strong>get in touch</strong></h3>
								<hr />
							</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beening the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book.</p>
							<div class="footer-text">
								<ul>
									<li>
										<i class="pe-7s-map-marker"></i>
                                        <p>Mirpur 10, Dhaka</p>
                                        <p>Bangladesh 1216</p>
									</li>
									<li>
										<i class="pe-7s-call"></i>
                                        <p>+880 1741228971</p>
                                        <p>+880 1741228971</p>
									</li>
									<li>
										<i class="pe-7s-mail-open"></i>
                                        <p>info@dailytrendybd.com</p>
                                        <p>contact@dailytrendybd.com</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="new-customer">
							<div class="log-title">
								<h3><strong>Have a questions?</strong></h3>
								<hr />
							</div>
							<div class="custom-input">
								<form action="mail.php" method="post">
									<input type="text" name="name" placeholder="Name" />
									<input type="text" name="email" placeholder="Email Address" />
									<input type="text" name="subject" placeholder="Subject" />
									<div class="review-mess">
										<textarea rows="4" placeholder="Massage" name="message"></textarea>
									</div>
									<div class="submit-text">
										<a href="order-complete.php">send message</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="map-outer">
                            <!--Map Canvas-->
                            <div class="map-canvas"
                                 data-zoom="16"
                                 data-lat="23.8069318"
                                 data-lng="90.3687099"
                                 data-type="roadmap"
                                 data-hue="#ffc400"
                                 data-title="Daily Trendy BD"
                                 data-content="Mirpur 10, Dhaka, Bangladesh 1216<br><a href='mailto:contact@dailytrendybd.com'>contact@dailytrendybd.com</a>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</section>
		<!-- contact section end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyDshnyhvIG46wBVASnK1VgpHy5vdE6T-T4"></script>
        <script src="js/map-script.js"></script>
    </body>
</html>
