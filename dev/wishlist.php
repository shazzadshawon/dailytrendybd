<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Wishlist || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Wishlist</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>Wishlist</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- wishlist content section start -->
		<section class="pages wishlist-page section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="table-responsive">
							<table class="wishlist-table text-center">
								<thead>
									<tr>
										<th>Remove</th>
										<th>Items</th>
										<th>Price</th>
										<th>Stock Status </th>
										<th>Add To Cart</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><i class="pe-7s-close" title="Remove this product"></i></td>
										<td class="td-img text-left">
											<a href="#"><img src="img/cart/daily2.jpg" alt="Add Product" style="width: 72px; height: 108px;" /></a>
											<div class="items-dsc">
												<h5><a href="#">Product Title demo</a></h5>
												<p class="itemcolor">Color : <span>Blue</span></p>
												<p class="itemcolor">Size   : <span>SL</span></p>
											</div>
										</td>
										<td>&#2547;52.00</td>
										<td>In Stock</td>
										<td>
											<div class="submit-text">
												<a href="cart.php">Add to cart</a>
											</div>
										</td>
									</tr>
									<tr>
										<td><i class="pe-7s-close" title="Remove this product"></i></td>
										<td class="td-img text-left">
											<a href="#"><img src="img/cart/daily3.jpg" alt="Add Product" style="width: 72px; height: 108px;" /></a>
											<div class="items-dsc">
												<h5><a href="#">Product Title</a></h5>
												<p class="itemcolor">Color : <span>Blue</span></p>
												<p class="itemcolor">Size   : <span>SL</span></p>
											</div>
										</td>
										<td>&#2547;52.00</td>
										<td>In Stock</td>
										<td>
											<div class="submit-text">
												<a href="cart.php">Add to cart</a>
											</div>
										</td>
									</tr>
									<tr>
										<td><i class="pe-7s-close" title="Remove this product"></i></td>
										<td class="td-img text-left">
											<a href="#"><img src="img/cart/daily4.jpg" alt="Add Product" style="width: 72px; height: 108px;" /></a>
											<div class="items-dsc">
												<h5><a href="#">Product Title demo</a></h5>
												<p class="itemcolor">Color : <span>Blue</span></p>
												<p class="itemcolor">Size   : <span>SL</span></p>
											</div>
										</td>
										<td>&#2547;52.00</td>
										<td>In Stock</td>
										<td>
											<div class="submit-text">
												<a href="cart.php">Add to cart</a>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- wishlist content section end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
