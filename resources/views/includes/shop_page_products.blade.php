<div class="row">
    <div class="tab-content">
        <div class="tab-pane active fade in text-center" id="grid">

                @include('includes.shop_grid_view')
        </div>
        <!-- grid view end -->

        <!-- LIST VIEW START -->
        <div class="tab-pane list-view  fade in" id="list">
            <div class="col-xs-12">
                @include('includes.shop_list_view')
            </div>
        </div>
        <!-- list view end -->
    </div>
</div>