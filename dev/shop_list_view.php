<div class="single-product">
    <div class="row">
        <div class="col-md-4">
            <div class="product-img">
                <a href="product-detail.php"><img src="img/products/daily4.jpg" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">
                <h4><a href="product-detail.php">Product Title</a></h4>
                <h3>&#2547;52.00</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    <li><a href="#">06 Reviews</a></li>
                    <li><a href="#">add your review</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix">
                    <li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a href="wishlist.php"><i class="pe-7s-like"></i></a></li>
                    <li><a href="cart.php"><i class="pe-7s-repeat"></i></a></li>
                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="cart.php"><i class="pe-7s-cart"></i>add to cart</a>
            </div>
        </div>
    </div>
</div>
<div class="single-product">
    <div class="row">
        <div class="col-md-4">
            <div class="product-img">
                <a href="product-detail.php"><img src="img/products/daily3.jpg" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">
                <h4><a href="product-detail.php">Product Title</a></h4>
                <h3>&#2547;52.00</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    <li><a href="#">06 Reviews</a></li>
                    <li><a href="#">add your review</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix">
                    <li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a href="wishlist.php"><i class="pe-7s-like"></i></a></li>
                    <li><a href="cart.php"><i class="pe-7s-repeat"></i></a></li>
                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="cart.php"><i class="pe-7s-cart"></i>add to cart</a>
            </div>
        </div>
    </div>
</div>
<div class="single-product">
    <div class="row">
        <div class="col-md-4">
            <div class="product-img">
                <a href="product-detail.php"><img src="img/products/daily2.jpg" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">

                <h4><a href="product-detail.php">Product Title</a></h4>
                <h3>&#2547;52.00</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    <li><a href="#">06 Reviews</a></li>
                    <li><a href="#">add your review</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix">
                    <li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a href="wishlist.php"><i class="pe-7s-like"></i></a></li>
                    <li><a href="cart.php"><i class="pe-7s-repeat"></i></a></li>
                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="cart.php"><i class="pe-7s-cart"></i>add to cart</a>
            </div>
        </div>
    </div>
</div>

<div class="single-product">
    <div class="row">
        <div class="col-md-4">
            <div class="product-img">
                <a href="product-detail.php"><img src="img/products/daily4.jpg" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">

                <h4><a href="product-detail.php">Product Title</a></h4>
                <h3>&#2547;52.00</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    <li><a href="#">06 Reviews</a></li>
                    <li><a href="#">add your review</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix">
                    <li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a href="wishlist.php"><i class="pe-7s-like"></i></a></li>
                    <li><a href="cart.php"><i class="pe-7s-repeat"></i></a></li>
                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="cart.php"><i class="pe-7s-cart"></i>add to cart</a>
            </div>
        </div>
    </div>
</div>
<div class="single-product">
    <div class="row">
        <div class="col-md-4">
            <div class="product-img">
                <a href="product-detail.php"><img src="img/products/daily3.jpg" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">

                <h4><a href="product-detail.php">Product Title</a></h4>
                <h3>&#2547;52.00</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    <li><a href="#">06 Reviews</a></li>
                    <li><a href="#">add your review</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix">
                    <li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a href="wishlist.php"><i class="pe-7s-like"></i></a></li>
                    <li><a href="cart.php"><i class="pe-7s-repeat"></i></a></li>
                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="cart.php"><i class="pe-7s-cart"></i>add to cart</a>
            </div>
        </div>
    </div>
</div>
<div class="single-product">
    <div class="row">
        <div class="col-md-4">
            <div class="product-img">
                <a href="product-detail.php"><img src="img/products/daily2.jpg" alt="Product Title" /></a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-dsc">

                <h4><a href="product-detail.php">Product Title</a></h4>
                <h3>&#2547;52.00</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                <ul class="clearfix">
                    <li><a href="#">in Stock</a></li>
                    <li><a href="#">06 Reviews</a></li>
                    <li><a href="#">add your review</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <div class="por-dse clearfix">
                <ul class="other-btn clearfix">
                    <li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
                    <li><a href="wishlist.php"><i class="pe-7s-like"></i></a></li>
                    <li><a href="cart.php"><i class="pe-7s-repeat"></i></a></li>
                </ul>
            </div>
            <div class="por-dse add-to">
                <a href="cart.php"><i class="pe-7s-cart"></i>add to cart</a>
            </div>
        </div>
    </div>
</div>