<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\AddToCart;
use App\Category;
use App\Product;
use App\Customer;
use App\Order;
use App\ShippingAddress;
use DB;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function homn3e()
    {
        //
    }


    public function index()
    {
       $order = DB::table('orders')
               ->join('customers', 'orders.customer_id', '=', 'customers.id')
               ->select('orders.*', 'customers.customer_name')
              ->groupBy('order_number')
              ->orderby('order_date','desc')
               ->get(); 
       return view('backend.manage_order')->with('orders',$order);
    }



    public function store(Request $request)
    {
        $cart_count = DB::table('add_to_carts')->where('session_id', Session::getId())->get();
        //return $cart_count;

        if(count($cart_count)==0)
        {
            Session::flash('message','Please select atleast one product to order');
            return redirect()->back();
        }

         $order_number =  rand();
         
         $shipping_address = new ShippingAddress;
         $shipping_address->order_number= $order_number;
         $shipping_address->name= $request->name;
         $shipping_address->phone= $request->phone;
         $shipping_address->address= $request->address;
         $shipping_address->location= $request->address;
        
         if( $shipping_address->save()){
        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
        
        foreach ($addTocart as $cart){
            $order = new Order;
            $order->product_id = $cart->product_id;
            $order->customer_id = Session::get('customer_id');
            $order->product_name = $cart->product_name;
            $order->product_code = $cart->product_code;
            $order->product_price = $cart->product_price;
            $order->product_quantity = $cart->product_quantity;
            $order->size = $cart->size;
            $order->order_number = $order_number;
            $order->order_date = date('d-m-Y');
            $order->session_id = Session::getId();
            
            if($order->save()){
                AddToCart::where('id',$cart->id)->delete();
            }
        }
         }
          Session::flash('message', 'Your Order Has Been placed!');
            return Redirect::to('/shipping');
    }


    public function store_with_reg(Request $request)
    {
        //return $request->all();
        if($request->password != $request->confirm_password)
        {
            Session::flash('message','Confirm Password did not matched !!');
            return \redirect()->back();
        }
//        $this->validate($request,array(
//            'fname'=>'required|max:255'
//        ));
        //return $request->all();

        $customer_id = DB::table('customers')->insertGetId(
            [
                'customer_name' => $request->fname,
                'phone_number' => $request->phone,
                'address' => $request->address,
                'email_address' => $request->email,
                'password' => md5($request->password),

            ]
        );
       // var_dump($customer);
       // exit;


//        $cuntomer = new Customer;
//
//        $cuntomer->customer_name = $request->fname;
//        $cuntomer->phone_number = $request->phone;
//        $cuntomer->address = $request->address;
//        $cuntomer->email_address = $request->email;
//        $cuntomer->password = md5($request->password);

        if(!empty($customer_id)){
            $customer = DB::table('customers')->where('id',$customer_id)->first();
            $request->Session()->put('customer_name',$customer->customer_name);
            $request->Session()->put('customer_id',$customer->id);
            //return $request->all();
//            Session::flash('message','Registration Completed ....!');
//            return Redirect::to('/shipping');
        }else{
            Session::flash('message','Invalid info ! Please try Again');
            return \redirect()->back();
        }

        $cart_count = DB::table('add_to_carts')->where('session_id', Session::getId())->get();
        //return $cart_count;

        if(count($cart_count)==0)
        {
            Session::flash('message','Please select atleast one product to order');
            return redirect()->back();
        }
        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get();



         $order_number =  rand();

         $shipping_address = new ShippingAddress;
         $shipping_address->order_number = $order_number;
         $shipping_address->name= $request->fname;
         $shipping_address->phone= $request->phone;
         $shipping_address->address= $request->address;
         $shipping_address->location= $request->address;

         if( $shipping_address->save()){


        foreach ($addTocart as $cart){
            $order = new Order;
            $order->product_id = $cart->product_id;
            $order->customer_id = Session::get('customer_id');
            $order->product_name = $cart->product_name;
            $order->product_code = $cart->product_code;
            $order->product_price = $cart->product_price;
            $order->product_quantity = $cart->product_quantity;
            $order->size = $cart->size;
            $order->order_number = $order_number;
            $order->order_date = date('d-m-Y');
            $order->session_id = Session::getId();

            if($order->save()){
                AddToCart::where('id',$cart->id)->delete();
            }
        }
         }
          Session::flash('message', 'Your Order Has Been placed!');
            return Redirect::to('/');
    }



    public function show($id)
    {
        
    }
    
    public function ViewOrder($id)
    {
         $order = DB::table('orders')->where('order_number',$id)
               ->join('customers', 'orders.customer_id', '=', 'customers.id')
               ->select('orders.*', 'customers.*')
               ->groupBy('order_number')
               ->first(); 
        return view('backend.view_order')->with('orders',$order);
    }
    
    public function delivered($id)
    {
        $order = DB::table('orders')
                ->where('order_number', $id)
                ->get();
        foreach ($order as $oder_info) {

            $product_info = DB::table('products')
                    ->where('id', $oder_info->product_id)
                    ->first();
            $order_qty = $oder_info->product_quantity;
            $product_qty = $product_info->product_quantity;
            $qty = $product_qty-$order_qty;
            
             $product = Product::where('id',$oder_info->product_id)
                ->update(['product_quantity' =>$qty]);
             $order_status = Order::where('order_number',$id)
                ->update(['publication_status' =>1]);
        }
        Session::flash('success', 'Your Selected Order Has Been delivered Successfully..!');
        return Redirect::to('/view-order/' . $id);
    }
    
      public function refuse($id)
    {
        $order = DB::table('orders')
                ->where('order_number', $id)
                ->get();
        foreach ($order as $oder_info) {    
             $order_status = Order::where('order_number',$id)
                ->update(['publication_status' =>2]);
        }
        Session::flash('success', 'Your Selected Order Has Been Refuse Successfully..!');
        return Redirect::to('/view-order/' . $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::where('order_number',$id)->delete();
        Session::flash('success', 'Your Selected Order Has Been Deleted Successfully..!');
        return Redirect::to('/manage-order');
    }
}
