<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Shop || Daily Treandy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
		<?php require('header.php'); ?>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Products</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>shop</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
        <!-- collection section start -->
		<section class="collection-area section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="single-colect">
							<img src="img/collect/1.png" alt="" />
							<div class="link-icon">
								<a href="#"><i class="pe-7s-back"></i></a>
							</div>
						</div>
					</div>
					<div class="col-sm-4 text-center">
						<div class="colect-text ">
							<h4><a href="#">fashion collection</a></h4>
							<hr class="line" />
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="single-colect">
							<img src="img/collect/2.png" alt="" />
							<div class="link-icon">
								<a href="#"><i class="pe-7s-back"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- collection section end -->
		<!-- product-list-view-left content section start -->
		<section class="pages products-page section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3 text-center">
						<div class="sidebar left-sidebar">
							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>filter by</h4>
								</div>
							</div>
							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>price</h4>
								</div>
								<div class="range-slider text-bg clearfix">
									<form action="#" method="get">
										<label><input type="text" id="amount" readonly /></label>
										<input class="submit floatright" type="submit" value="Search">
										<div id="slider-range"></div>
									</form>
								</div>
							</div>
							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>color</h4>
								</div>
								<div class="color text-bg">
									<ul>
										<li><a href="#">read</a></li>
										<li><a href="#">green</a></li>
										<li><a href="#">blue</a></li>
										<li><a href="#">yellow</a></li>
										<li><a href="#">gray</a></li>
										<li><a href="#">orange</a></li>
									</ul>
								</div>
							</div>
							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>MANUFACTURER</h4>
								</div>
								<div class="facturer text-bg">
									<ul>
										<li><a href="#">Consequat <span class="floatright">12</span></a></li>
										<li><a href="#">Sollicitudin <span class="floatright">25</span></a></li>
										<li><a href="#">Pellentesque <span class="floatright">65</span></a></li>
										<li><a href="#">premium label <span class="floatright">89</span></a></li>
										<li><a href="#">TOMMY HILFIGER <span class="floatright">65</span></a></li>
									</ul>
								</div>
							</div>
							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>size</h4>
								</div>
								<div class="size text-bg">
									<ul>
										<li><a href="#">ssl</a></li>
										<li><a href="#">xl</a></li>
										<li><a href="#">sl</a></li>
										<li><a href="#">xl</a></li>
										<li><a href="#">m</a></li>
										<li><a href="#">s</a></li>
										<li><a href="#">l</a></li>
									</ul>
								</div>
							</div>
							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>CATEGORY</h4>
								</div>
								<div class="facturer category text-bg">
									<ul>
										<li><a href="#">Fashion & accessories <span class="floatright">12</span></a></li>
										<li><a href="#">Product types <span class="floatright">25</span></a></li>
										<li><a href="#">women fashion <span class="floatright">65</span></a></li>
										<li><a href="#">men fashion <span class="floatright">89</span></a></li>
										<li><a href="#">baby fashion <span class="floatright">65</span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-9">
						<div class="right-products">
							<div class="row">
								<div class="col-xs-12">
									<div class="section-title clearfix">
										<ul>
											<li>
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#grid"> <img src="img/products/grid-icon.png" alt="grid-icon" /> </a></li>
													<li class="active"><a data-toggle="tab" href="#list"> <img src="img/products/list-icon.png" alt="list-icon" /> </a></li>
												</ul>
											</li>
											<li class="sort-by l-r-margin">
												<h5>sort by :</h5>
											</li>
											<li class="sort-by">
												<form action="mail.php" method="post">
													<div class="custom-select">
														<select class="form-control">
															<option> Newest Products</option>
															<option> men Products </option>
															<option> women Products </option>
															<option> popular Products </option>
															<option> best sell Products </option>
														</select>
													</div>
												</form>
											</li>
											<li class="sort-by l-r-margin">
												<h5>show :</h5>
											</li>
											<li class="sort-by">
												<form action="mail.php" method="post">
													<div class="custom-select">
														<select class="form-control">
															<option> 09</option>
															<option> 10</option>
															<option> 11</option>
															<option> 12</option>
															<option> 15</option>
															<option> 16</option>
															<option> 18</option>
														</select>
													</div>
												</form>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="tab-content">
									<div class="tab-pane  fade in text-center" id="grid">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<div class="pro-type">
														<span>sale</span>
													</div>
													<a href="#"><img src="img/products/6.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span>$52.00</span>
												</div>
												<div class="actions-btn">
													<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<div class="pro-type">
														<span>new</span>
													</div>
													<a href="#"><img src="img/products/9.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span><del>$65.00</del>$52.00</span>
												</div>
												<div class="actions-btn">
													<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<a href="#"><img src="img/products/5.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span>$52.00</span>
												</div>
												<div class="actions-btn">
													<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<a href="#"><img src="img/products/4.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span><del>$65.00</del>$52.00</span>
												</div>
												<div class="actions-btn">
													<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<a href="#"><img src="img/products/7.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span><del>$65.00</del>$52.00</span>
												</div>
												<div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<a href="#"><img src="img/products/8.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span><del>$65.00</del>$52.00</span>
												</div>
												<div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<div class="pro-type">
														<span>new</span>
													</div>
													<a href="#"><img src="img/products/1.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span>$72.00</span>
												</div>
												<div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="single-product">
												<div class="product-img">
													<a href="#"><img src="img/products/2.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span><del>$65.00</del>$52.00</span>
												</div>
												<div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
										<div class="col-xs-12 hidden-sm col-md-4">
											<div class="single-product">
												<div class="product-img">
													<div class="pro-type">
														<span>sell</span>
													</div>
													<a href="#"><img src="img/products/3.png" alt="Product Title" /></a>
												</div>
												<div class="product-dsc">
													<p><a href="#">Product Title</a></p>
													<span><del>$65.00</del>$52.00</span>
												</div>
												<div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
													<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
												</div>
											</div>
										</div>
										<!-- single product end -->
									</div>
									<!-- grid view end -->
									<div class="tab-pane list-view active fade in" id="list">
										<div class="col-xs-12">
											<div class="single-product">
												<div class="row">
													<div class="col-md-4">
														<div class="product-img">
															<div class="pro-type">
																<span>sale</span>
															</div>
															<a href="#"><img src="img/products/3.png" alt="Product Title" /></a>
														</div>
													</div>
													<div class="col-md-6">
														<div class="product-dsc">
															<div class="rating floatright">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
															</div>
															<h4><a href="#">Product Title</a></h4>
															<h3>$52.00</h3>
															<p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
															<ul class="clearfix">
																<li><a href="#">in Stock</a></li>
																<li><a href="#">06 Reviews</a></li>
																<li><a href="#">add your review</a></li>
															</ul>
														</div>
													</div>
													<div class="col-md-2">
														<div class="por-dse clearfix">
															<ul class="other-btn clearfix">
																<li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
																<li><a href="#"><i class="pe-7s-like"></i></a></li>
																<li><a href="#"><i class="pe-7s-repeat"></i></a></li>
																<li><a href="#">ssl</a></li>
																<li><a href="#">xs</a></li>
																<li><a href="#">xl</a></li>
															</ul>
														</div>
														<div class="por-dse add-to">
															<a href="#"><i class="pe-7s-cart"></i>add to cart</a>
														</div>
													</div>
												</div>
											</div>
											<div class="single-product">
												<div class="row">
													<div class="col-md-4">
														<div class="product-img">
															<a href="#"><img src="img/products/4.png" alt="Product Title" /></a>
														</div>
													</div>
													<div class="col-md-6">
														<div class="product-dsc">
															<div class="rating floatright">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
															</div>
															<h4><a href="#">Product Title</a></h4>
															<h3>$52.00</h3>
															<p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
															<ul class="clearfix">
																<li><a href="#">in Stock</a></li>
																<li><a href="#">06 Reviews</a></li>
																<li><a href="#">add your review</a></li>
															</ul>
														</div>
													</div>
													<div class="col-md-2">
														<div class="por-dse clearfix">
															<ul class="other-btn clearfix">
																<li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
																<li><a href="#"><i class="pe-7s-like"></i></a></li>
																<li><a href="#"><i class="pe-7s-repeat"></i></a></li>
																<li><a href="#">ssl</a></li>
																<li><a href="#">xs</a></li>
																<li><a href="#">xl</a></li>
															</ul>
														</div>
														<div class="por-dse add-to">
															<a href="#"><i class="pe-7s-cart"></i>add to cart</a>
														</div>
													</div>
												</div>
											</div>
											<div class="single-product">
												<div class="row">
													<div class="col-md-4">
														<div class="product-img">
															<div class="pro-type">
																<span>new</span>
															</div>
															<a href="#"><img src="img/products/5.png" alt="Product Title" /></a>
														</div>
													</div>
													<div class="col-md-6">
														<div class="product-dsc">
															<div class="rating floatright">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
															</div>
															<h4><a href="#">Product Title</a></h4>
															<h3>$52.00</h3>
															<p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
															<ul class="clearfix">
																<li><a href="#">in Stock</a></li>
																<li><a href="#">06 Reviews</a></li>
																<li><a href="#">add your review</a></li>
															</ul>
														</div>
													</div>
													<div class="col-md-2">
														<div class="por-dse clearfix">
															<ul class="other-btn clearfix">
																<li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
																<li><a href="#"><i class="pe-7s-like"></i></a></li>
																<li><a href="#"><i class="pe-7s-repeat"></i></a></li>
																<li><a href="#">ssl</a></li>
																<li><a href="#">xs</a></li>
																<li><a href="#">xl</a></li>
															</ul>
														</div>
														<div class="por-dse add-to">
															<a href="#"><i class="pe-7s-cart"></i>add to cart</a>
														</div>
													</div>
												</div>
											</div>
											<div class="single-product">
												<div class="row">
													<div class="col-md-4">
														<div class="product-img">
															<a href="#"><img src="img/products/6.png" alt="Product Title" /></a>
														</div>
													</div>
													<div class="col-md-6">
														<div class="product-dsc">
															<div class="rating floatright">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
															</div>
															<h4><a href="#">Product Title</a></h4>
															<h3>$52.00</h3>
															<p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
															<ul class="clearfix">
																<li><a href="#">in Stock</a></li>
																<li><a href="#">06 Reviews</a></li>
																<li><a href="#">add your review</a></li>
															</ul>
														</div>
													</div>
													<div class="col-md-2">
														<div class="por-dse clearfix">
															<ul class="other-btn clearfix">
																<li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
																<li><a href="#"><i class="pe-7s-like"></i></a></li>
																<li><a href="#"><i class="pe-7s-repeat"></i></a></li>
																<li><a href="#">ssl</a></li>
																<li><a href="#">xs</a></li>
																<li><a href="#">xl</a></li>
															</ul>
														</div>
														<div class="por-dse add-to">
															<a href="#"><i class="pe-7s-cart"></i>add to cart</a>
														</div>
													</div>
												</div>
											</div>
											<div class="single-product">
												<div class="row">
													<div class="col-md-4">
														<div class="product-img">
															<div class="pro-type">
																<span>new</span>
															</div>
															<a href="#"><img src="img/products/9.png" alt="Product Title" /></a>
														</div>
													</div>
													<div class="col-md-6">
														<div class="product-dsc">
															<div class="rating floatright">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
															</div>
															<h4><a href="#">Product Title</a></h4>
															<h3>$52.00</h3>
															<p>Lorem Ipsum is simply dummy text of the printing and types setting industry. Lorem Ipsum has been the industry's stan dard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
															<ul class="clearfix">
																<li><a href="#">in Stock</a></li>
																<li><a href="#">06 Reviews</a></li>
																<li><a href="#">add your review</a></li>
															</ul>
														</div>
													</div>
													<div class="col-md-2">
														<div class="por-dse clearfix">
															<ul class="other-btn clearfix">
																<li><a href="#" data-toggle="modal" data-target="#quick-view"><i class="pe-7s-expand1"></i></a></li>
																<li><a href="#"><i class="pe-7s-like"></i></a></li>
																<li><a href="#"><i class="pe-7s-repeat"></i></a></li>
																<li><a href="#">ssl</a></li>
																<li><a href="#">xs</a></li>
																<li><a href="#">xl</a></li>
															</ul>
														</div>
														<div class="por-dse add-to">
															<a href="#"><i class="pe-7s-cart"></i>add to cart</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- list view end -->
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="blog-pagination">
										<nav>
											<ul class="pagination">
												<li>
													<a href="#" aria-label="Previous">
														<span aria-hidden="true">prev</span>
													</a>
												</li>
												<li class="active"><a href="#">1</a></li>
												<li><a href="#">2</a></li>
												<li><a href="#">3</a></li>
												<li><a href="#">4</a></li>
												<li>
													<a href="#" aria-label="Next">
														<span aria-hidden="true">next</span>
													</a>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- product-list-view-left content section end -->
        <!-- quick view start -->
		<div class="quick-view modal fade in" id="quick-view">
			<div class="container">
				<div class="row">
					<div id="view-gallery" class="owl-carousel product-slider owl-theme">
						<div class="col-xs-12">
							<div class="d-table">
								<div class="d-tablecell">
									<div class="modal-dialog">
										<div class="main-view modal-content">
											<div class="modal-footer" data-dismiss="modal">
												<span>x</span>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-5">
													<div class="quick-image">
														<div class="single-quick-image tab-content text-center">
															<div class="tab-pane  fade in active" id="sin-pro-1">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-2">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-3">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-4">
																<img src="img/quick-view/l.png" alt="" />
															</div>
														</div>
														<div class="quick-thumb">
															<div class="nav nav-tabs">
																<ul>
																	<li><a data-toggle="tab" href="#sin-pro-1"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-2"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-3"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-4"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																</ul>
															</div>
														</div>
													</div>							
												</div>
												<div class="col-xs-12 col-sm-7">
													<div class="quick-right">
														<div class="quick-right-text">
															<h3><strong>product name title</strong></h3>
															<div class="rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
																<a href="#">06 Review</a>
																<a href="#">Add review</a>
															</div>
															<div class="amount">
																<h4>$65.00</h4>
															</div>
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beenin the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
															<div class="row m-p-b">
																<div class="col-sm-12 col-md-6">
																	<div class="por-dse responsive-strok clearfix">
																		<ul>
																			<li><span>Availability</span><strong>:</strong> In stock</li>
																			<li><span>Condition</span><strong>:</strong> New product</li>
																			<li><span>Category</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
																<div class="col-sm-12 col-md-6">
																	<div class="por-dse color">
																		<ul>
																			<li><span>color</span><strong>:</strong> <a href="#">Red</a> <a href="#">Green</a> <a href="#">Blue</a> <a href="#">Orange</a></li>
																			<li><span>size</span><strong>:</strong>  <a href="#">SL</a> <a href="#">SX</a> <a href="#">M</a> <a href="#">XL</a></li>
																			<li><span>tag</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="dse-btn">
																<div class="row">
																	<div class="col-sm-12 col-md-6">
																		<div class="por-dse clearfix">
																			<ul>
																				<li class="share-btn qty clearfix"><span>quantity</span>
																					<form action="#" method="POST">
																						<div class="plus-minus">
																							<a class="dec qtybutton">-</a>
																							<input type="text" value="02" name="qtybutton" class="plus-minus-box">
																							<a class="inc qtybutton">+</a>
																						</div>
																					</form>
																				</li>
																				<li class="share-btn clearfix"><span>share</span>
																					<a href="#"><i class="fa fa-facebook"></i></a>
																					<a href="#"><i class="fa fa-twitter"></i></a>
																					<a href="#"><i class="fa fa-google-plus"></i></a>
																					<a href="#"><i class="fa fa-linkedin"></i></a>
																					<a href="#"><i class="fa fa-instagram"></i></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-sm-12 col-md-6">
																		<div class="por-dse clearfix responsive-othre">
																			<ul class="other-btn">
																				<li><a href="#"><i class="fa fa-heart"></i></a></li>
																				<li><a href="#"><i class="fa fa-refresh"></i></a></li>
																				<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
																			</ul>
																		</div>
																		<div class="por-dse add-to">
																			<a href="#">add to cart</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- single-product item end -->
						<div class="col-xs-12">
							<div class="d-table">
								<div class="d-tablecell">
									<div class="modal-dialog">
										<div class="main-view modal-content">
											<div class="modal-footer" data-dismiss="modal">
												<span>x</span>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-5">
													<div class="quick-image">
														<div class="single-quick-image tab-content text-center">
															<div class="tab-pane  fade in active" id="sin-pro-5">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-6">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-7">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-8">
																<img src="img/quick-view/l.png" alt="" />
															</div>
														</div>
														<div class="quick-thumb">
															<div class="nav nav-tabs">
																<ul>
																	<li><a data-toggle="tab" href="#sin-pro-5"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-6"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-7"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-8"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																</ul>
															</div>
														</div>
													</div>							
												</div>
												<div class="col-xs-12 col-sm-7">
													<div class="quick-right">
														<div class="quick-right-text">
															<h3><strong>product name title</strong></h3>
															<div class="rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
																<a href="#">06 Review</a>
																<a href="#">Add review</a>
															</div>
															<div class="amount">
																<h4>$65.00</h4>
															</div>
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beenin the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
															<div class="row m-p-b">
																<div class="col-sm-6">
																	<div class="por-dse">
																		<ul>
																			<li><span>Availability</span><strong>:</strong> In stock</li>
																			<li><span>Condition</span><strong>:</strong> New product</li>
																			<li><span>Category</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="por-dse color">
																		<ul>
																			<li><span>color</span><strong>:</strong> <a href="#">Red</a> <a href="#">Green</a> <a href="#">Blue</a> <a href="#">Orange</a></li>
																			<li><span>size</span><strong>:</strong>  <a href="#">SL</a> <a href="#">SX</a> <a href="#">M</a> <a href="#">XL</a></li>
																			<li><span>tag</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="dse-btn">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="por-dse">
																			<ul>
																				<li class="share-btn qty clearfix"><span>quantity</span>
																					<form action="#" method="POST">
																						<div class="plus-minus">
																							<a class="dec qtybutton">-</a>
																							<input type="text" value="02" name="qtybutton" class="plus-minus-box">
																							<a class="inc qtybutton">+</a>
																						</div>
																					</form>
																				</li>
																				<li class="share-btn clearfix"><span>share</span>
																					<a href="#"><i class="fa fa-facebook"></i></a>
																					<a href="#"><i class="fa fa-twitter"></i></a>
																					<a href="#"><i class="fa fa-google-plus"></i></a>
																					<a href="#"><i class="fa fa-linkedin"></i></a>
																					<a href="#"><i class="fa fa-instagram"></i></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="por-dse clearfix">
																			<ul class="other-btn">
																				<li><a href="#"><i class="fa fa-heart"></i></a></li>
																				<li><a href="#"><i class="fa fa-refresh"></i></a></li>
																				<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
																			</ul>
																		</div>
																		<div class="por-dse add-to">
																			<a href="#">add to cart</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- single-product item end -->
					</div>
				</div>
			</div>
		</div>
		<!-- quick view end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
