<section class="new-products single-products section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>new arrivals</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div id="new-products" class="owl-carousel product-slider owl-theme">
                <div class="col-xs-12">
                    <div class="single-product">
                        <div class="product-img">
                            <div class="pro-type">
                                <span>sale</span>
                            </div>
                            <a href="product-detail.php"><img class="new_arrival_height" src="img/products/daily2.jpg" alt="Product Title" /></a>
                        </div>
                        <div class="product-dsc">
                            <p><a href="product-detail.php">Product Title</a></p>
                            <span>&#2547;52.00</span>
                        </div>
                        <div class="actions-btn">
                            <a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                            <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                        </div>
                    </div>
                </div>
                <!-- single product end -->
                <div class="col-xs-12">
                    <div class="single-product">
                        <div class="product-img">
                            <div class="pro-type">
                                <span>new</span>
                            </div>
                            <a href="product-detail.php"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
                        </div>
                        <div class="product-dsc">
                            <p><a href="product-detail.php">Product Title</a></p>
                            <span>&#2547;52.00</span>
                        </div>
                        <div class="actions-btn">
                            <a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                            <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                        </div>
                    </div>
                </div>
                <!-- single product end -->
                <div class="col-xs-12">
                    <div class="single-product">
                        <div class="product-img">
                            <a href="product-detail.php"><img class="new_arrival_height" src="img/products/daily4.jpg" alt="Product Title" /></a>
                        </div>
                        <div class="product-dsc">
                            <p><a href="product-detail.php">Product Title</a></p>
                            <span>&#2547;52.00</span>
                        </div>
                        <div class="actions-btn">
                            <a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                            <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                        </div>
                    </div>
                </div>
                <!-- single product end -->
                <div class="col-xs-12">
                    <div class="single-product">
                        <div class="product-img">
                            <a href="product-detail.php"><img class="new_arrival_height" src="img/products/daily3.jpg" alt="Product Title" /></a>
                        </div>
                        <div class="product-dsc">
                            <p><a href="product-detail.php">Product Title</a></p>
                            <span>&#2547;52.00</span>
                        </div>
                        <div class="actions-btn">
                            <a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                            <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                        </div>
                    </div>
                </div>
                <!-- single product end -->
                <div class="col-xs-12">
                    <div class="single-product">
                        <div class="product-img">
                            <div class="pro-type">
                                <span>new</span>
                            </div>
                            <a href="product-detail.php"><img class="new_arrival_height" src="img/products/daily2.jpg" alt="Product Title" /></a>
                        </div>
                        <div class="product-dsc">
                            <p><a href="product-detail.php">Product Title</a></p>
                            <span>&#2547;52.00</span>
                        </div>
                        <div class="actions-btn">
                            <a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                            <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                        </div>
                    </div>
                </div>
                <!-- single product end -->
            </div>
        </div>
    </div>
</section>