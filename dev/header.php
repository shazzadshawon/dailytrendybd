<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="left-header clearfix">
                        <ul>
                            <li><p><i class="pe-7s-call"></i>(+880) 1910 000251</p></li>
                            <li><p><i class="pe-7s-clock"></i>Mon-fri : 9:00-19:00</p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="right-header">
                        <ul>
                            <li><i class="pe-7s-search"></i>
                                <form method="get" id="searchform" action="#">
                                    <input type="text" value="" name="s" id="ws" placeholder="Search product..." />
                                    <button type="submit"><i class="pe-7s-search"></i></button>
                                </form>
                            </li>
                            <li><a href="#"><i class="pe-7s-settings"></i></a>
                                <ul class="drop-setting">
                                    <li><a href="my-account.php"><i class="fa fa-user"></i>My account</a></li>
                                    <li><a href="cart.php"><i class="fa fa-shopping-cart"></i>My cart</a></li>
                                    <li><a href="wishlist.php"><i class="fa fa-heart"></i>My wishlist</a></li>
                                    <li><a href="checkout.php"><i class="fa">&#2547;</i>Check out</a></li>
                                    <li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
                                </ul>
                            </li>
                            <li><a href="cart.php"><i class="pe-7s-cart"></i><span class="color1">2</span></a>
                                <?php require('header_cart.php'); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom header-bottom-one" id="sticky-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-md-2">
                    <div class="logo">
                        <a href="index.php"><img src="img/daily_logo_a.png" alt="Daily Trendy BD" /></a>
                    </div>
                </div>
                <div class="col-sm-10 col-md-10">
                    <div class="mainmenu clearfix">
                        <nav>
                            <?php require('menu_content.php'); ?>
                        </nav>
                    </div>
                    <!-- mobile menu start -->
                        <?php require('menu_content_mobile.php'); ?>
                    <!-- mobile menu end -->
                </div>
            </div>
        </div>
    </div>
</header>