<section class="featured-products single-products section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>FEATURED PRODUCTS</h3>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <div class="product-tab nav nav-tabs">
                    <ul>
                        <li class="active"><a data-toggle="tab" href="#all">all shop</a></li>
                        <li><a data-toggle="tab" href="#clothings">clothings</a></li>
                        <li><a data-toggle="tab" href="#shoes">shoes</a></li>
                        <li><a data-toggle="tab" href="#bags">bags</a></li>
                        <li><a data-toggle="tab" href="#accessories">accessories</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row text-center tab-content">
            <div class="tab-pane  fade in active" id="all">
                <div id="tab-carousel-1" class="owl-carousel product-slider owl-theme">
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <div class="pro-type">
                                    <span>sale</span>
                                </div>
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <div class="pro-type">
                                    <span>new</span>
                                </div>
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                </div>
            </div>
            <!-- all shop product end -->
            <div class="tab-pane  fade in" id="clothings">
                <div id="tab-carousel-2" class="owl-carousel product-slider owl-theme">
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <div class="pro-type">
                                    <span>new</span>
                                </div>
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <div class="pro-type">
                                    <span>sale</span>
                                </div>
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->
                </div>
            </div>
            <!-- clothings product end -->
            <div class="tab-pane  fade in" id="shoes">
                <div id="tab-carousel-3" class="owl-carousel product-slider owl-theme">
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->
                </div>
            </div>
            <!-- shoes product end -->
            <div class="tab-pane  fade in" id="bags">
                <div id="tab-carousel-4" class="owl-carousel product-slider owl-theme">
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->
                </div>
            </div>
            <!-- bags product end -->
            <div class="tab-pane  fade in" id="accessories">
                <div id="tab-carousel-5" class="owl-carousel product-slider owl-theme">
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily2.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span>&#2547;42.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                        <div class="single-product margin-top">
                            <div class="product-img">
                                <a href="product-detail.php"><img class="featured_product_imgHeight" src="img/products/daily3.jpg" alt="Product Title" /></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="product-detail.php">Product Title</a></p>
                                <span><del>&#2547;65.00</del>&#2547;52.00</span>
                            </div>
                            <div class="actions-btn"><a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
                                <a href="wishlist.php" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- single product end -->
                </div>
            </div>
            <!-- accessories product end -->
        </div>
    </div>
</section>