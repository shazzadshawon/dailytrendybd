<section class="new-products single-products section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>new arrivals</h3>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div id="new-products" class="owl-carousel product-slider owl-theme">
                <?php $x=1; ?>
                @foreach($new_arraivals as $new)

                    <?php
                    $image = DB::table('product_images')->where('product_id',$new->id)->orderBy('product_image_id','desc')->first();
                    $images = DB::table('product_images')->where('product_id',$new->id)->orderBy('product_image_id','desc')->get();
                    ?>

                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <div class="pro-type">
                                    {{--<span>sale</span>--}}

                                </div>
                                @if(!empty($image))
                                    <a href="{{url('product-details/'.$new->id)}}"><img class="new_arrival_height" src="{{asset('product_image/'.$image->product_image)}}" alt="Product Title" /></a>
                                @endif
                            </div>
                            <div class="product-dsc">
                                <p><a href="{{url('product-details/'.$new->id)}}">{{$new->product_name}}</a></p>
                                <span>&#2547; @if($new->discount > 0){{ $new->product_price-($new->product_price*$new->discount)/100}}@else{{$new->product_price}}@endif</span>
                            </div>
                            <div class="actions-btn">
                                {{--<a href="#" data-toggle="modal" data-trigger="hover" data-target="#{{$new->id}}" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>--}}

                                {!! Form::open(['route' => 'wishlist.store','files'=>true,'id'=>$new->id]) !!}
                                <input type="hidden" name="session_id"
                                       value="{{Session::getId()}}">
                                <input type="hidden" name="product_id"
                                       value="{{$new->id}}">


                                {!! Form::close() !!}


                                <a href="" onclick="document.getElementById({{$new->id}}).submit(); " data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>


                                {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'id'=>$x]) !!}

                                <input type="hidden" name="product_id" value="{{$new->id}}">
                                <input type="hidden" name="product_name" value="{{$new->product_name}}">
                                <input type="hidden" name="product_name_bn"
                                       value="{{$new->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$new->product_code}}">
                                <input type="hidden" name="product_price"
                                       value="@if($new->discount > 0){{ $new->product_price-($new->product_price*$new->discount)/100}}@else{{$new->product_price}}@endif">
                                <input type="hidden" name="publication_status"
                                       value="{{$new->publication_status}}">

                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="size" value="0">


                                {!! Form::close() !!}


                                <a href="" onclick="document.getElementById({{$x}}).submit();" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
                            </div>
                        </div>
                    </div>

                @include('includes.quick_view_slide')

                    <?php
                    $x++;
                    ?>


            @endforeach
            <!-- quick view start -->



                <!-- quick view end -->
            </div>
        </div>
    </div>
</section>