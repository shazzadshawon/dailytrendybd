<section class="testimonials section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>TESTIMONIALS</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-text-center">
                <div id="testimonials" class="owl-carousel product-slider owl-theme">
                    @foreach($reviews as $review)
                        <div class="single-testimonial">
                            <div class="testimonial-img">
                                <a href="#"><img style="height: 200px;" src="{{asset('public/uploads/review/'.$review->review_image)}}" alt="Title" /></a>
                            </div>
                            <div class="testimonial-dsc">
                                <h4><a href="#">{{$review->review_title}}</a></h4>
                                <span>{{$review->created_at}}</span>
                                <?php
                                    print_r($review->review_description);
                                ?>
                            </div>
                        </div>
                @endforeach
                    <!-- single testimonial item end -->

                </div>
            </div>
        </div>
    </div>
</section>