<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Checkout || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Checkout</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>Checkout</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
		<!-- Checkout content section start -->
		<section class="pages checkout login-page section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="login-text billing new-customer">
							<div class="log-title">
								<h3><strong>billing details</strong></h3>
								<hr />
							</div>
							<div class="custom-input">
								<form action="mail.php" method="post">
									<input type="text" name="name" placeholder="Name" />
									<input type="text" name="email" placeholder="Email Address" />
									<input type="text" name="number" placeholder="Phone Number" />
									<div class="custom-select">
										<select class="form-control">
											<option> Country</option>
											<option> Bangladesh </option>
											<option> United States </option>
											<option> United Kingdom </option>
											<option> Canada </option>
											<option> Malaysia </option>
											<option> United Arab Emirates </option>
										</select>
									</div>
									<div class="custom-select">
										<select class="form-control">
											<option> Town / City</option>
											<option> Aberdeen </option>
											<option> Bedfordshire </option>
											<option> Caerphilly </option>
											<option> Denbighshire </option>
											<option> East Ayrshire </option>
											<option> Falkirk </option>
										</select>
									</div>
									<input type="text" name="address" placeholder="Address" />
									<input type="text" name="address" placeholder="Password" />
									<input type="password" name="password" placeholder="Confirm Password" />
									<div class="review-mess">
										<textarea rows="4" placeholder="Order notes here" name="message"></textarea>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="log-title">
							<h3><strong>your order</strong></h3>
							<hr />
						</div>
						<div class="place-text clearfix">
							<table>
								<thead>
									<tr>
										<th>product</th>
										<td>total</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>men shoes x 01 <span>Color : Red & Size : 9</span></th>
										<td>&#2547;56.00</td>
									</tr>
									<tr>
										<th>women shoes x 02<span>Color : Red & Size : 8</span></th>
										<td>&#2547;120.00</td>
									</tr>
									<tr>
										<th>shipping  & handling</th>
										<td>&#2547;10.00</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>sub total</th>
										<td>&#2547;186.00</td>
									</tr>
								</tfoot>
							</table>
							<ul id="accordion" class="panel-group clearfix">
								<li class="panel">
									<div data-toggle="collapse" data-parent="#accordion" data-target="#collapse1">
										<label>
											<input type="radio" name="rememberme" value="forever" checked="">
											direct bank transfer
										</label>
									</div>
									<div class="panel-collapse collapse in" id="collapse1">
										<div class="prayment-dsc">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
										</div>
									</div>
								</li>
								<li class="panel panelimg">
									<div data-toggle="collapse" data-parent="#accordion" data-target="#collapse2">
										<label>
											<input type="radio" name="rememberme" value="forever">
											cheque payment
										</label>
									</div>
									<div class="paypal-dsc panel-collapse collapse" id="collapse2">
										<div class="prayment-dsc">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
										</div>
									</div>
								</li>
								<li class="panel panelimg">
									<div data-toggle="collapse" data-parent="#accordion" data-target="#collapse3">
										<label>
											<input type="radio" name="rememberme" value="forever">
											paypal
										</label>
									</div>
									<div id="collapse3" class="paypal-dsc panel-collapse collapse">
										<div class="prayment-dsc">
											<p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
										</div>
									</div>
								</li>
							</ul>
							<div class="submit-text place-order floatright">
								<a href="order-complete.php">place order</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Checkout content section end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
