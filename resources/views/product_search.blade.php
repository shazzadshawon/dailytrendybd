@extends('layouts.frontend')

@section('content')
    <div class="pages-title about-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="pages-title-text">
                        <h3>About Us</h3>
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><span>/</span>Search</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- category-slider -->
              
                <!-- ./category-slider -->
                <!-- subcategories -->
             
                <!-- ./subcategories -->
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list" style="margin-bottom: 50px">
                    <h2 class="page-heading">
                        <span class="page-heading-title"> 
                                Search Results :
                              </span>
                    </h2>
                    {{-- <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul> --}}
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">

                    
                            @if(count($products) == 0)
                            <br><br>
                            <h4>No Item Found !</h4>
                          
                            @endif

                         
                            @foreach ($products as $productsInfo)
                                @php
                                    $productsImage = DB::table('product_images')->where('product_id',$productsInfo->id)->first();
                                @endphp
                            <li class="col-sx-12 col-sm-3">
                                <div class="left-block">
                                    <a href="{{ URL::to('/product-details/'.$productsInfo->id) }}"><img class="img-responsive" alt="product" src="{{ asset('product_image/'.$productsImage->product_image) }}" style="height: 250px;" /></a>
                                    <!-------------Wishlist-------------->
                               @php
                                 
                                        $wishlist = DB::table('wishlists')
                                                ->where('product_id', $productsInfo->id)
                                                ->where('customer_id', Session::get('customer_id'))->first();
                               @endphp
                                          
                                   

                               
                            <!--------------wishlist--------- -->
                                   {{--  <div class="add-to-cart">
                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                    </div> --}}
                                </div>
                                <div class="right-block">




                                    <div class="product-dsc">
                                        <p><a href="{{url('product-details/'.$productsInfo->id)}}"> {{ $productsInfo->product_name }}</a></p>
                                        <span>&#2547; @if($productsInfo->discount > 0){{ $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100}}@else{{$productsInfo->product_price}}@endif</span>
                                    </div>
                                        


                                </div>
                            </li>
                            @endforeach
       
                        
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
      {{--           <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                          <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                              <a href="#" aria-label="Next">
                                <span aria-hidden="true">Next &raquo;</span>
                              </a>
                            </li>
                          </ul>
                        </nav>
                    </div>
                    <div class="show-product-item">
                        <select name="">
                            <option value="">Show 18</option>
                            <option value="">Show 20</option>
                            <option value="">Show 50</option>
                            <option value="">Show 100</option>
                        </select>
                    </div>
                    <div class="sort-product">
                        <select>
                            <option value="">Product Name</option>
                            <option value="">Price</option>
                        </select>
                        <div class="sort-product-icon">
                            <i class="fa fa-sort-alpha-asc"></i>
                        </div>
                    </div>
                </div> --}}
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection