<?php

$carts = DB::table('add_to_carts')->where('session_id',\Session::getId())->get();

$total =0;
?>


<ul class="drop-cart">

    @foreach ($carts as $cart)
        @php
            $product = DB::table('products')->where('id',$cart->product_id)->first();
            $image = DB::table('product_images')->where('product_id',$product->id)->first();

        @endphp
    <li>
         @if(!empty($image))
        <a href="{{url('product-details/'.$product->id)}}"><img style="height: 50px;" src="{{asset('product_image/'.$image->product_image)}}" alt="" /></a>
        @endif
        <div class="add-cart-text">
            <p><a href="#">{{$product->product_name}}</a></p>
            <small>{{ $cart->product_price }} X {{ $cart->product_quantity }}</small>
            <p>&#2547; {{ $cart->product_price * $cart->product_quantity}}</p>

            <span>Size  :{{$cart->size}}</span>
        </div>

        @php
            $total = $cart->product_price * $cart->product_quantity + $total;
        @endphp


        {{--<div class="pro-close">--}}
            {{--<i class="pe-7s-close"></i>--}}
        {{--</div>--}}
    </li>
    @endforeach

    <li class="total-amount clearfix">
        <span class="floatleft">total</span>
        <span class="floatright"><strong>= &#2547; {{$total}}</strong></span>
    </li>
    <li>
        <div class="goto text-center">
            <a href="{{url('cart')}}"><strong>go to cart &nbsp;<i class="pe-7s-angle-right"></i></strong></a>
        </div>
    </li>
    <li class="checkout-btn text-center">
        <a href="{{url('shipping')}}">Check out</a>
    </li>
</ul>