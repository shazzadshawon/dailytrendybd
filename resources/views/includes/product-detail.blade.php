<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Product Details || Daily Treandy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
		<?php require('header.php'); ?>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Product Details</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span><a href="product-grid-left.html">Shop</a></li>
								<li><span>/</span>Product Details</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
        <!-- product-details-section-start -->
		<div class="quick-view product-details section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-12 col-sm-5">
								<div class="quick-image">
									<div class="single-quick-image tab-content text-center">
										<div class="simpleLens-container tab-pane  fade in" id="sin-1">
											<a class="simpleLens-image" data-lens-image="img/products/sim-l.png" href="#"><img src="img/products/tab-l.png" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade active in" id="sin-2">
											<a class="simpleLens-image" data-lens-image="img/products/sim-l.png" href="#"><img src="img/products/tab-l.png" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-3">
											<a class="simpleLens-image" data-lens-image="img/products/sim-l.png" href="#"><img src="img/products/tab-l.png" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-4">
											<a class="simpleLens-image" data-lens-image="img/products/sim-l.png" href="#"><img src="img/products/tab-l.png" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-5">
											<a class="simpleLens-image" data-lens-image="img/products/sim-l.png" href="#"><img src="img/products/tab-l.png" alt="" class="simpleLens-big-image"></a>
										</div>
										<div class="simpleLens-container tab-pane fade in" id="sin-6">
											<a class="simpleLens-image" data-lens-image="img/products/sim-l.png" href="#"><img src="img/products/tab-l.png" alt="" class="simpleLens-big-image"></a>
										</div>
									</div>
									<div class="quick-thumb">
										<div class="nav nav-tabs">
											<ul id="tabs-details" class="owl-carousel product-slider owl-theme">
												<li class="tablist"><a data-toggle="tab" href="#sin-1"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
												<li class="tablist active"><a  class="active" data-toggle="tab" href="#sin-2"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-3"> <img src="img/quick-view/s.png" alt="small image" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-4"> <img src="img/quick-view/s.png" alt="small image" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-5"> <img src="img/quick-view/s.png" alt="small image" /> </a></li>
												<li class="tablist"><a data-toggle="tab" href="#sin-6"> <img src="img/quick-view/s.png" alt="small image" /> </a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-7">
								<div class="quick-right">
									<div class="quick-right-text">
										<h3><strong>product name title</strong></h3>
										<div class="rating">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-half-o"></i>
											<i class="fa fa-star-o"></i>
											<a href="#">06 Review</a>
											<a href="#">Add review</a>
										</div>
										<div class="amount">
											<h4>$65.00</h4>
										</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beenin the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
										<div class="row m-p-b">
											<div class="col-sm-12 col-md-6">
												<div class="por-dse">
													<ul>
														<li><span>Availability</span><strong>:</strong> In stock</li>
														<li><span>Condition</span><strong>:</strong> New product</li>
														<li><span>Category</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
													</ul>
												</div>
											</div>
											<div class="col-sm-12 col-md-6">
												<div class="por-dse color">
													<ul>
														<li><span>color</span><strong>:</strong> <a href="#">Red</a> <a href="#">Green</a> <a href="#">Blue</a> <a href="#">Orange</a></li>
														<li><span>size</span><strong>:</strong>  <a href="#">SL</a> <a href="#">SX</a> <a href="#">M</a> <a href="#">XL</a></li>
														<li><span>tag</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="dse-btn">
											<div class="row">
												<div class="col-sm-7 col-md-6">
													<div class="por-dse">
														<ul>
															<li class="share-btn qty clearfix"><span>quantity</span>
																<form action="#" method="POST">
																	<div class="plus-minus">
																		<a class="dec qtybutton">-</a>
																		<input type="text" value="02" name="qtybutton" class="plus-minus-box">
																		<a class="inc qtybutton">+</a>
																	</div>
																</form>
															</li>
															<li class="share-btn clearfix"><span>share</span>
																<a href="#"><i class="fa fa-facebook"></i></a>
																<a href="#"><i class="fa fa-twitter"></i></a>
																<a href="#"><i class="fa fa-google-plus"></i></a>
																<a href="#"><i class="fa fa-linkedin"></i></a>
																<a href="#"><i class="fa fa-instagram"></i></a>
															</li>
														</ul>
													</div>
												</div>
												<div class="col-sm-5 col-md-6">
													<div class="por-dse clearfix responsive-othre">
														<ul class="other-btn">
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
															<li><a href="#"><i class="fa fa-refresh"></i></a></li>
															<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
														</ul>
													</div>
													<div class="por-dse add-to">
														<a href="#">add to cart</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- single-product item end -->
				</div>
				<!-- reviews area start -->
				<div class="reviews">
					<div class="row">
						<div class="col-xs-12">
							<div class="reviews-tab">
								<ul id="reviews-tab" class="nav">
									<li><a href="#moreinfo">moreinfo</a></li>
									<li class="active"><a href="#reviews">Reviews</a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="info-reviews review-text tab-pane fade in" id="moreinfo">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi. Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus, ultricies posuere erat. Duis convallis, arcu nec aliquam consequat, purus felis vehicula felis, a dapibus enim lorem nec augue.</p>
								</div>
								<div class="info-reviews review-text tab-pane fade in active" id="reviews">
									<h5>1 review for Accumsan Elit</h5>
									<div class="autohr-text">
										<img src="img/blog/author3.png" alt="" />
										<div class="author-des">
											<h4>Sarah Bailey</h4>
											<span class="floatright">Jun 25, 2016 at 2:50pm</span>
											<span class="verygood">Very Good</span>
											<div class="rating">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-o"></i>
												<i class="fa fa-star-o"></i>
											</div>
										</div>
									</div>
									<div class="your-rating">
										<h5>Your rating</h5>
										<div class="rating clearfix">
											<ul>
												<li>
													<a href="#">
														<i class="fa fa-star-o"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="custom-input">
										<form action="mail.php" method="post">
											<div class="row">
												<div class="col-sm-4">
													<div class="input-mail">
														<input type="text" name="name" placeholder="Your Name" />
														<input type="text" name="email" placeholder="Your Email" />
														<input type="text" name="review" placeholder="Summary of your review..." />
													</div>
													<div class="submit-text">
														<button type="submit">submit review</button>
													</div>
												</div>
												<div class="col-sm-8">
													<div class="review-mess">
														<textarea name="message" placeholder="Your Review" rows="5"></textarea>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- reviews area end -->
			</div>
		</div>
		<!-- product-details section end -->
        <!-- new-products section start -->
		<section class="new-products single-products section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="section-title">
							<h3>related products</h3>
						</div>
					</div>
				</div>
				<div class="row text-center">
					<div id="new-products" class="owl-carousel product-slider owl-theme">
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<div class="pro-type">
										<span>sale</span>
									</div>
									<a href="#"><img src="img/products/1.png" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>$52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<div class="pro-type">
										<span>new</span>
									</div>
									<a href="#"><img src="img/products/2.png" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>$52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/products/3.png" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>$52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/products/4.png" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>$52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<div class="pro-type">
										<span>new</span>
									</div>
									<a href="#"><img src="img/products/5.png" alt="Product Title" /></a>
								</div>
								<div class="product-dsc">
									<p><a href="#">Product Title</a></p>
									<span>$52.00</span>
								</div>
								<div class="actions-btn">
									<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view" data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i class="pe-7s-like"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="pe-7s-repeat"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i class="pe-7s-cart"></i></a>
								</div>
							</div>
						</div>
						<!-- single product end -->
					</div>
				</div>
			</div>
		</section>
		<!-- new-products section end -->
        <!-- quick view start -->
		<div class="quick-view modal fade in" id="quick-view">
			<div class="container">
				<div class="row">
					<div id="view-gallery" class="owl-carousel product-slider owl-theme">
						<div class="col-xs-12">
							<div class="d-table">
								<div class="d-tablecell">
									<div class="modal-dialog">
										<div class="main-view modal-content">
											<div class="modal-footer" data-dismiss="modal">
												<span>x</span>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-5">
													<div class="quick-image">
														<div class="single-quick-image tab-content text-center">
															<div class="tab-pane  fade in active" id="sin-pro-1">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-2">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-3">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-4">
																<img src="img/quick-view/l.png" alt="" />
															</div>
														</div>
														<div class="quick-thumb">
															<div class="nav nav-tabs">
																<ul>
																	<li><a data-toggle="tab" href="#sin-pro-1"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-2"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-3"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-4"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																</ul>
															</div>
														</div>
													</div>							
												</div>
												<div class="col-xs-12 col-sm-7">
													<div class="quick-right">
														<div class="quick-right-text">
															<h3><strong>product name title</strong></h3>
															<div class="rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
																<a href="#">06 Review</a>
																<a href="#">Add review</a>
															</div>
															<div class="amount">
																<h4>$65.00</h4>
															</div>
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beenin the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
															<div class="row m-p-b">
																<div class="col-sm-12 col-md-6">
																	<div class="por-dse responsive-strok clearfix">
																		<ul>
																			<li><span>Availability</span><strong>:</strong> In stock</li>
																			<li><span>Condition</span><strong>:</strong> New product</li>
																			<li><span>Category</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
																<div class="col-sm-12 col-md-6">
																	<div class="por-dse color">
																		<ul>
																			<li><span>color</span><strong>:</strong> <a href="#">Red</a> <a href="#">Green</a> <a href="#">Blue</a> <a href="#">Orange</a></li>
																			<li><span>size</span><strong>:</strong>  <a href="#">SL</a> <a href="#">SX</a> <a href="#">M</a> <a href="#">XL</a></li>
																			<li><span>tag</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="dse-btn">
																<div class="row">
																	<div class="col-sm-12 col-md-6">
																		<div class="por-dse clearfix">
																			<ul>
																				<li class="share-btn qty clearfix"><span>quantity</span>
																					<form action="#" method="POST">
																						<div class="plus-minus">
																							<a class="dec qtybutton">-</a>
																							<input type="text" value="02" name="qtybutton" class="plus-minus-box">
																							<a class="inc qtybutton">+</a>
																						</div>
																					</form>
																				</li>
																				<li class="share-btn clearfix"><span>share</span>
																					<a href="#"><i class="fa fa-facebook"></i></a>
																					<a href="#"><i class="fa fa-twitter"></i></a>
																					<a href="#"><i class="fa fa-google-plus"></i></a>
																					<a href="#"><i class="fa fa-linkedin"></i></a>
																					<a href="#"><i class="fa fa-instagram"></i></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-sm-12 col-md-6">
																		<div class="por-dse clearfix responsive-othre">
																			<ul class="other-btn">
																				<li><a href="#"><i class="fa fa-heart"></i></a></li>
																				<li><a href="#"><i class="fa fa-refresh"></i></a></li>
																				<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
																			</ul>
																		</div>
																		<div class="por-dse add-to">
																			<a href="#">add to cart</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- single-product item end -->
						<div class="col-xs-12">
							<div class="d-table">
								<div class="d-tablecell">
									<div class="modal-dialog">
										<div class="main-view modal-content">
											<div class="modal-footer" data-dismiss="modal">
												<span>x</span>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-5">
													<div class="quick-image">
														<div class="single-quick-image tab-content text-center">
															<div class="tab-pane  fade in active" id="sin-pro-5">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-6">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-7">
																<img src="img/quick-view/l.png" alt="" />
															</div>
															<div class="tab-pane fade in" id="sin-pro-8">
																<img src="img/quick-view/l.png" alt="" />
															</div>
														</div>
														<div class="quick-thumb">
															<div class="nav nav-tabs">
																<ul>
																	<li><a data-toggle="tab" href="#sin-pro-5"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-6"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-7"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																	<li><a data-toggle="tab" href="#sin-pro-8"> <img src="img/quick-view/s.png" alt="quick view" /> </a></li>
																</ul>
															</div>
														</div>
													</div>							
												</div>
												<div class="col-xs-12 col-sm-7">
													<div class="quick-right">
														<div class="quick-right-text">
															<h3><strong>product name title</strong></h3>
															<div class="rating">
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star"></i>
																<i class="fa fa-star-half-o"></i>
																<i class="fa fa-star-o"></i>
																<a href="#">06 Review</a>
																<a href="#">Add review</a>
															</div>
															<div class="amount">
																<h4>$65.00</h4>
															</div>
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beenin the stand ard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrames bled it make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
															<div class="row m-p-b">
																<div class="col-sm-6">
																	<div class="por-dse">
																		<ul>
																			<li><span>Availability</span><strong>:</strong> In stock</li>
																			<li><span>Condition</span><strong>:</strong> New product</li>
																			<li><span>Category</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="por-dse color">
																		<ul>
																			<li><span>color</span><strong>:</strong> <a href="#">Red</a> <a href="#">Green</a> <a href="#">Blue</a> <a href="#">Orange</a></li>
																			<li><span>size</span><strong>:</strong>  <a href="#">SL</a> <a href="#">SX</a> <a href="#">M</a> <a href="#">XL</a></li>
																			<li><span>tag</span><strong>:</strong> <a href="#">Men</a> <a href="#">Fashion</a> <a href="#">Shirt</a></li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="dse-btn">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="por-dse">
																			<ul>
																				<li class="share-btn qty clearfix"><span>quantity</span>
																					<form action="#" method="POST">
																						<div class="plus-minus">
																							<a class="dec qtybutton">-</a>
																							<input type="text" value="02" name="qtybutton" class="plus-minus-box">
																							<a class="inc qtybutton">+</a>
																						</div>
																					</form>
																				</li>
																				<li class="share-btn clearfix"><span>share</span>
																					<a href="#"><i class="fa fa-facebook"></i></a>
																					<a href="#"><i class="fa fa-twitter"></i></a>
																					<a href="#"><i class="fa fa-google-plus"></i></a>
																					<a href="#"><i class="fa fa-linkedin"></i></a>
																					<a href="#"><i class="fa fa-instagram"></i></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="por-dse clearfix">
																			<ul class="other-btn">
																				<li><a href="#"><i class="fa fa-heart"></i></a></li>
																				<li><a href="#"><i class="fa fa-refresh"></i></a></li>
																				<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
																			</ul>
																		</div>
																		<div class="por-dse add-to">
																			<a href="#">add to cart</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- single-product item end -->
					</div>
				</div>
			</div>
		</div>
		<!-- quick view end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
