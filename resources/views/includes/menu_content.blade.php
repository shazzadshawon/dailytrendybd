<ul>
    <li><a href="{{url('/')}}">Home</a></li>
    <li><a href="">Shop</a>
        <div class="magamenu">
            <ul id="megamenu_category">
                <?php
                $i=0;
                ?>
                @foreach($main_categories as $cat)
                <li class="right-mega colmd12 @if($i==0) active @endif"><a href="#">{{$cat->category_name}}</a>
                    <ul class="megamenu_subcategory">
                        @php
                            $sub_categories = DB::table('sub_categories')->where('category_id',$cat->category_id)->get();
                        @endphp

                        @foreach($sub_categories as $sub)
                        <li class="single-menu colmd4">
                            <a href="{{url('Category-products/'.$sub->sub_category_id)}}">{{$sub->sub_category_name}}</a>

                        </li>

                        @endforeach

                    </ul>
                </li>
                        <?php
                        $i++;
                        ?>
                @endforeach

            </ul>
        </div>
    </li>
    <li><a href="{{url('about')}}">About</a></li>
    <li><a href="{{url('contact')}}">Contact</a></li>
</ul>

<script type="text/javascript">
    $(window).load(function(){
        var cat_h = $('#megamenu_category').outerHeight();
        console.log(cat_h);
        if(cat_h > 500){
            $('.megamenu_subcategory').css('height', cat_h+'px');
        }
    });
</script>