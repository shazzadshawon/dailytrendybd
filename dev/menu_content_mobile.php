<div class="mobile-menu-area">
    <div class="mobile-menu">
        <nav id="dropdown">
            <ul>
                <li><a href="index.php">Home</a>
                    <ul>
                        <li><a href="index.php">Home Version One</a></li>
                    </ul>
                </li>
                <li><a href="shop.php">Shop</a>
                    <ul>
                        <li><a href="#">all products</a>
                            <ul>
                                <li>
                                    <span>men’s wear</span>
                                    <a href="#">shirts & top</a>
                                    <a href="#">shoes</a>
                                    <a href="#">dresses</a>
                                    <a href="#">shwmwear</a>
                                    <a href="#">jeans</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>women’s wear</span>
                                    <a href="#">shirts & tops</a>
                                    <a href="#">shoes</a>
                                    <a href="#">dresses</a>
                                    <a href="#">shwmwear</a>
                                    <a href="#">jeans</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>accessories</span>
                                    <a href="#">sunglasses</a>
                                    <a href="#">leather</a>
                                    <a href="#">belts</a>
                                    <a href="#">rings</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">persess</a>
                                    <a href="#">bags</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">New products</a>
                            <ul>
                                <li>
                                    <span>men’s wear</span>
                                    <a href="#">shirts & top</a>
                                    <a href="#">shoes</a>
                                    <a href="#">jeans</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>women’s wear</span>
                                    <a href="#">shirts & tops</a>
                                    <a href="#">shoes</a>
                                    <a href="#">dresses</a>
                                    <a href="#">shwmwear</a>
                                    <a href="#">jeans</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>accessories</span>
                                    <a href="#">sunglasses</a>
                                    <a href="#">leather</a>
                                    <a href="#">belts</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">persess</a>
                                    <a href="#">bags</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">best sell</a>
                            <ul>
                                <li>
                                    <span>men’s wear</span>
                                    <a href="#">shirts & top</a>
                                    <a href="#">shoes</a>
                                    <a href="#">dresses</a>
                                    <a href="#">shwmwear</a>
                                    <a href="#">jeans</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>accessories</span>
                                    <a href="#">sunglasses</a>
                                    <a href="#">leather</a>
                                    <a href="#">belts</a>
                                    <a href="#">rings</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">persess</a>
                                    <a href="#">bags</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">features products</a>
                            <ul>
                                <li>
                                    <span>men’s wear</span>
                                    <a href="#">shirts & top</a>
                                    <a href="#">shoes</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>women’s wear</span>
                                    <a href="#">shirts & tops</a>
                                    <a href="#">shoes</a>
                                    <a href="#">dresses</a>
                                    <a href="#">jacket</a>
                                </li>
                                <li>
                                    <span>accessories</span>
                                    <a href="#">sunglasses</a>
                                    <a href="#">leather</a>
                                    <a href="#">belts</a>
                                    <a href="#">rings</a>
                                    <a href="#">sweaters</a>
                                    <a href="#">persess</a>
                                    <a href="#">bags</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
    </div>
</div>