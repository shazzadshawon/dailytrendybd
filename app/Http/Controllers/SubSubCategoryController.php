<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Session;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use DB;

class SubSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SubSubCategory = DB::table('sub_sub_categories')
                ->join('sub_categories', 'sub_sub_categories.sub_category_id', '=', 'sub_categories.sub_category_id')               
                ->select('sub_sub_categories.*', 'sub_categories.sub_category_name')
                ->get();
        return view('admin.pages.manage_sub_sub_category')->with('SubSubCategories',$SubSubCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add_sub_sub_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,array(
            'sub_category_id'=>'required',            
           'sub_sub_category_name'=>'required|max:255'
       ));
        $sub_category = new SubSubCategory;
      
       $sub_category->sub_category_id = $request->sub_category_id;
       $sub_category->sub_sub_category_name = $request->sub_sub_category_name;
       $sub_category->sub_sub_category_name_bn = $request->sub_sub_category_name_bn;
       $sub_category->publication_status = $request->publication_status;
       $sub_category->save();
       if($sub_category){
           Session::flash('message','Sub Category has been Saved Successfully ....!');
        return Redirect::to('/add-sub-sub-category');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $SubCategories = SubSubCategory::where('id',$id)->first();
        //$category = Category::where('publication_status',1)->get();
//        // return the view and pass in the var we previously created
//        return view('admin.pages.edit_sub_category')->withSubCategory($SubCategories)->withCategory($category);
        
           // find the post in the database and save as a var
       // $sub_category = SubCategory::where('sub_category_id',$id)->first();
        // return the view and pass in the var we previously created
        return view('admin.pages.edit_sub_sub_category')->withSubSubCategory($SubCategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'sub_category_id' => 'required',
            'sub_sub_category_name' => 'required|max:255'
        ));
        $sub_category = SubSubCategory::where('id', $id)
                ->update(['sub_category_id' => $request->sub_category_id,
            'sub_sub_category_name' => $request->sub_sub_category_name,
            'sub_sub_category_name_bn' => $request->sub_sub_category_name_bn,
            'publication_status' => $request->publication_status
        ]);

        Session::flash('message', 'Sub Category Has Been Updated Successfully..!');
        return Redirect::to('/manage-sub-sub-category');
    }
  

     public function unpublished($id) {

//        $category = new Category;
        $sub_category = SubSubCategory::where('id', $id)
                ->update(['publication_status' => 0]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();


        Session::flash('message', 'Sub Category Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-sub-sub-category');
    }
    
      public function published( $id)
    {
   
//        $category = new Category;
        $sub_category = SubSubCategory::where('id',$id)
                ->update(['publication_status' =>1]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();
      
        
            Session::flash('message', 'Sub Category Has Been published Successfully..!');
            return Redirect::to('/manage-sub-sub-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubSubCategory::where('id',$id)->delete();
        Session::flash('message', 'Your Selected Sub Category Has Been Deleted Successfully ....!');
            return Redirect::to('/manage-sub-sub-category');
    }
}
