@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->








<div class="pages-title section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="pages-title-text">
                    <h3>Wishlist</h3>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><span>/</span>Wishlist</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- pages-title-end -->
<!-- wishlist content section start -->
<section class="pages wishlist-page section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="wishlist-table text-center">
                        <thead>
                        <tr>

                            <th>Items</th>
                            <th>Price</th>
                            <th>Stock Status </th>
                            <th>Remove</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $wishlist = DB::table('wishlists')
                        ->orderBy('id', 'desc')
                        ->where('session_id', Session::getId())->get();

                        foreach ($wishlist as $wishlist_info){
                        $product_id=$wishlist_info->product_id;
                        $product= DB::table('products')
                        ->where('id', $product_id)->first();
                        $product_image= DB::table('product_images')
                        ->where('product_id', $product_id)->first();
                        ?>

                        <tr>

                            <td class="td-img text-left">
                                <a href="{{url('product-details/'.$product->id)}}"><img style="width: 100px; height: 100px;" src="{{asset('product_image/'.$product_image->product_image)}}" alt="Add Product" style="width: 72px; height: 108px;" /></a>
                                <div class="items-dsc">
                                    <h5><a href="{{url('product-details/'.$product->id)}}">{{$product->product_name}}</a></h5>


                                </div>
                            </td>
                            <td>&#2547;@if($product->discount > 0){{ $product->product_price-($product->product_price*$product->discount)/100}}@else{{$product->product_price}}@endif</td>
                            <td>@if($product->product_quantity > 0) In Stock @else Stocked Out @endif</td>
                            <td><a href="{{url('remove-wishlist/'.$wishlist_info->id)}}"><i class="pe-7s-close" title="Remove this product"></i></a></td>
                        </tr>


                        <?php }?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection