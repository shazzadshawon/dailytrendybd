<div class="row">
    <div class="tab-content">
        <div class="tab-pane  fade in text-center" id="grid">
            <?php require('shop_grid_view.php'); ?>
        </div>
        <!-- grid view end -->

        <!-- LIST VIEW START -->
        <div class="tab-pane list-view active fade in" id="list">
            <div class="col-xs-12">
                <?php require('shop_list_view.php'); ?>
            </div>
        </div>
        <!-- list view end -->
    </div>
</div>