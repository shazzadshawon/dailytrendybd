@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
<div class="" style="padding: 0;">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <ul>
                        <li>{!! Session::get('message') !!}</li>
                    </ul>
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <ul>
                        <li>{!! Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            @if (Session::has('info'))
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <ul>
                        <li>{!! Session::get('info') !!}</li>
                    </ul>
                </div>
            @endif
            @if (Session::has('warning'))
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <ul>
                        <li>{!! Session::get('warning') !!}</li>
                    </ul>
                </div>
            @endif
            @if (Session::has('danger'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <ul>
                        <li>{!! Session::get('danger') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
           
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
               
                     <div class="page-content">
            <div class="row">
                <div class="col-sm-6">
                      {!! Form::open(['route' => 'customer.store']) !!}
                    <div class="box-authentication">
                        <h3>Create an account</h3>
                        <p>Please enter your email address to create an account.</p>
                        <label for="emmail_register">Name</label>
                        <input id="emmail_register" type="text" class="form-control" name="customer_name">
                        <label for="emmail_register">Phone Number</label>
                        <input id="emmail_register" type="text" class="form-control" name="phone_number">
                        <label for="emmail_register">Address</label>                        
                        <textarea id="emmail_register" name="address" class="form-control"></textarea>
                        <label for="emmail_register">Email address</label>
                        <input id="emmail_register" type="email" name="email_address" class="form-control">
                        <label for="emmail_register">Password</label>
                        <input id="emmail_register" type="password" name="password" class="form-control">
                    
                        <button class="button"><i class="fa fa-user"></i> Sign up</button>
                    </div>
                   {!! Form::close() !!}
                </div>
                <div class="col-sm-6">
                    <div class="box-authentication">
                        <h3>Already registered?</h3>
                        {!! Form::open(['url' => '/customer-login-check', 'method'=>'POST']) !!}
                        <label for="emmail_login">Email address</label>
                        <input id="emmail_login" type="text" class="form-control" name="email_address">
                        <label for="password_login">Password</label>
                        <input id="password_login" type="password" class="form-control" name="password">
                        {{-- <p class="forgot-pass"><a href="#">Forgot your password?</a></p> --}}
                        <button class="button"><i class="fa fa-lock"></i> Login</button>
                      {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
                </div>
                <!-- ./view-product-list-->
      
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
@endsection