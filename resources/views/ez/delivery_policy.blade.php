@extends('layouts.frontend')

@section('content')
<!-- HEADER -->


<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{url('/')}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Delivery Information</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Infomations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                     <li><span></span><a href="{{ url('about-us') }}">About Us</a></li>
                                    <li  class="active"><span></span><a href="{{ url('delivery_policy') }}">Delivery Information</a></li>
                                    <li><span></span><a href="{{ url('contact-us') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">Delivery Information</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">
                    <div class="panel panel-success">
                        <div class="panel-heading"><h2>Free Shipping</h2></div>
                        <div class="panel-body">
                            Delivery Details: Delivery Within 1-5 business days (Delivery Charge: Dhaka- Free, Outside Dhaka: 65 TK
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading"><h2>Cash on Delivery is available in the Following Metropolitan Cities</h2></div>
                        <div class="panel-body">
                            <strong> Dhaka </strong> <br>
                            <strong> Sylhet </strong> <br> 
                            <strong> Chittagong </strong> <br>
                            <strong> Khulna </strong> <br>
                            <strong> Bogra </strong> <br>
                            <strong> Comilla </strong> <br>
                            <strong> Rajshahi </strong>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading"><h2>Delivery Time</h2></div>
                        <div class="panel-body">
                            <ul>
                                <li>In Dhaka the product delivery takes 24 hours and in some cases it may take up to 48 Hours, customers inside Dhaka can also avail Same day delivery service if they are under the Same Day Delivery area</li>
                                <li>In case of outside Dhaka 3-5 Workings days is required for proper delivery of the product. It may vary due to Political/Natural disturbances in the country</li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading"><h2>Return Policy</h2></div>
                        <div class="panel-body">
                            <ul>
                                <li>Customer can easily return the product to the delivery person when the delivery is being made</li>
                                <li>In case of electronic gadgets the seal can’t be broken</li>
                                <li>Customer can also send the product back through courier service to our office address</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection