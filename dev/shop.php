<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Shop || Daily Trendy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <div id="anotherPage">
            <?php require('header.php'); ?>
        </div>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>Products</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>shop</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
        <!-- offer section start -->
        <?php require('offer.php'); ?>
        <!-- offer section end -->
		<!-- product-list-view-left content section start -->
		<section class="pages products-page section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3 text-center">
						<div class="sidebar left-sidebar">

							<div class="s-side-text">
								<div class="sidebar-title">
									<h4>CATEGORY</h4>
								</div>
								<div class="facturer category text-bg">
									<ul>
										<li><a href="#">Fashion & accessories <span class="floatright">12</span></a></li>
										<li><a href="#">Product types <span class="floatright">25</span></a></li>
										<li><a href="#">women fashion <span class="floatright">65</span></a></li>
										<li><a href="#">men fashion <span class="floatright">89</span></a></li>
										<li><a href="#">baby fashion <span class="floatright">65</span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-9">
						<div class="right-products">
							<div class="row">
								<div class="col-xs-12">
									<div class="section-title clearfix">
										<ul>
											<li>
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#grid"> <img src="img/products/grid-icon.png" alt="grid-icon" /> </a></li>
													<li class="active"><a data-toggle="tab" href="#list"> <img src="img/products/list-icon.png" alt="list-icon" /> </a></li>
												</ul>
											</li>
											<li class="sort-by l-r-margin">
												<h5>sort by :</h5>
											</li>
											<li class="sort-by">
												<form action="mail.php" method="post">
													<div class="custom-select">
														<select class="form-control">
															<option> Newest Products</option>
															<option> men Products </option>
															<option> women Products </option>
															<option> popular Products </option>
															<option> best sell Products </option>
														</select>
													</div>
												</form>
											</li>
											<li class="sort-by l-r-margin">
												<h5>show :</h5>
											</li>
											<li class="sort-by">
												<form action="mail.php" method="post">
													<div class="custom-select">
														<select class="form-control">
															<option> 09</option>
															<option> 10</option>
															<option> 11</option>
															<option> 12</option>
															<option> 15</option>
															<option> 16</option>
															<option> 18</option>
														</select>
													</div>
												</form>
											</li>
										</ul>
									</div>
								</div>
							</div>

                            <?php require('shop_page_products.php'); ?>

<!--							<div class="row">-->
<!--								<div class="col-xs-12">-->
<!--									<div class="blog-pagination">-->
<!--										<nav>-->
<!--											<ul class="pagination">-->
<!--												<li>-->
<!--													<a href="#" aria-label="Previous">-->
<!--														<span aria-hidden="true">prev</span>-->
<!--													</a>-->
<!--												</li>-->
<!--												<li class="active"><a href="#">1</a></li>-->
<!--												<li><a href="#">2</a></li>-->
<!--												<li><a href="#">3</a></li>-->
<!--												<li><a href="#">4</a></li>-->
<!--												<li>-->
<!--													<a href="#" aria-label="Next">-->
<!--														<span aria-hidden="true">next</span>-->
<!--													</a>-->
<!--												</li>-->
<!--											</ul>-->
<!--										</nav>-->
<!--									</div>-->
<!--								</div>-->
<!--							</div>-->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- product-list-view-left content section end -->
        <!--START QUICK VIEW MODAL-->
        <?php require('quick_view.php'); ?>
        <!--END QUICK VIEW MODAL-->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->
        <?php require('tail.php'); ?>
    </body>
</html>
