@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif --}}

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{url('/')}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Contact</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Contact Us</span>
        </h2>
        <!-- ../page heading-->
        <div id="contact" class="page-content page-contact">
            <div id="message-box-conact"></div>
            <div class="row">
                <form action="{{ url('post_contact') }}" method="POST">
                {{ csrf_field() }}
                    <div class="col-sm-6">
                        <h3 class="page-subheading">CONTACT FORM</h3>
                        <div class="contact-form-box">
                            <div class="form-selector">
                                <label>Subject Heading</label>
                                <select class="form-control input-sm" id="subject" name="contact_title">
                                    <option value="Customer service">Customer service</option>
                                    <option value="Webmaster">Webmaster</option>
                                </select>
                            </div>
                            <div class="form-selector">
                                <label>Email address</label>
                                <input type="text" class="form-control input-sm" id="email"  name="contact_email" />
                            </div>
                            <div class="form-selector">
                                <label>Order reference</label>
                                <input type="text" class="form-control input-sm" id="order_reference"  name="contact_reference"/>
                            </div>
                            <div class="form-selector">
                                <label>Message</label>
                                <textarea class="form-control input-sm" rows="10" id="message"  name="contact_description"></textarea>
                            </div>
                            <div class="form-selector">
                                <button id="btn-send-contact" type="submit" class="btn">Send</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-xs-12 col-sm-6" id="contact_form_map">
                    <h3 class="page-subheading">Information</h3>
                    <p>For any questions or feedback, please contact our Customer Service Here and we will try our best to respond to your inquiry within 24 hours.</p>
                    <br/>
                    <ul class="store_info">
                        <li><i class="fa fa-home"></i>Lakes Point Office: MRC Bhaban, Mirpur-1, Mirpur, Dhaka(1216),Bangladesh
                        </li>
                        <li><i class="fa fa-phone"></i><span>01611392222</span></li>
                        <li><i class="fa fa-phone"></i><span>Whats,app: 01790504256</span></li>
                        <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:lakespoint@gmail.com">lakespoint@gmail.com</a></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection