@extends('layouts.frontend')

@section('content')
    <div class="pages-title about-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="pages-title-text">
                        <h3>About Us</h3>
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><span>/</span>About Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="about-img">
                        <img src="{{asset('public/frontend/img/about/l.jpg')}}" alt="" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="about-text">
                        <div class="about-author">
                            <h3>We are crazy fashion</h3>
                            <hr />
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempores incidi dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercing tation ullamco laboris nisi ut aliquip ex ea commodo consequats. Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt culp qui offici deserunt mollit anim id est laborum.</p>
                        <ul>
                            <li><i>Who you are</i></li>
                            <li><i>Why you sell the items you sell</i></li>
                            <li><i>Where you are located</i></li>
                            <li><i>How long you have been in business</i></li>
                        </ul>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dalese orem que laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veri tatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about-us section end -->
    <!-- team-member section start -->

@endsection