<section class="collection-area section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title" style="text-align: center">
                    <h3>offers</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="blog" class="owl-carousel product-slider owl-theme">
                @foreach($offers as $offer)
                 @if(!empty($offer))
                <div class="col-xs-12">
                    <div class="single-blog">
                        <div class="s-blog-img clearfix">
                            <div class="blog-img">
                                <a href=""><img style="height: 300px;" class="offer_img_height" src="{{asset('pazzle_image/'.$offer->pazzle_image)}}" alt=""/></a>
                            </div>
                        </div>
                        {{--<div class="blog-dsc">--}}
                            {{--<p><a href="">offer title</a></p>--}}
                            {{--<p>Offer description Offer description  Offer description Offer description Offer description Offer description Offer description </p>--}}
                        {{--</div>--}}
                    </div>
                </div>
                @endif
                @endforeach
                <!-- single blog item end -->

            </div>
        </div>
    </div>
</section>