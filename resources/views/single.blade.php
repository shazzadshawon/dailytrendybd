@extends('layouts.frontend')

@section('content')

    @php
        $ct = DB::table('categories')->where('category_id',$productInfo->category_id)->first();
        $images = DB::table('product_images')->where('product_id',$productInfo->id)->orderBy('product_image_id','desc')->get();
    @endphp
    {{--$productInfo->product_name--}}

    <div class="pages-title section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="pages-title-text">
                        <h3>Product Details</h3>
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><span>/</span><a href="">Shop</a></li>
                            <li><span>/</span>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- pages-title-end -->
    <!-- product-details-section-start -->
    <div class="quick-view product-details section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5">
                            <div class="quick-image">
                                <div class="single-quick-image tab-content text-center">
                                    @php
                                        $i=0;
                                    $wishlist = DB::table('wishlists')
                            ->where('product_id', $productInfo->id)
                            ->where('session_id', \Session::getId())->first();
                                    @endphp
                                    @foreach($images as $image)

                                        <div class="simpleLens-container tab-pane  fade @if($i==0) active @endif in"
                                             id="{{$image->product_image_id}}">
                                            <a class="simpleLens-image"
                                               data-lens-image="{{asset('product_image/'.$image->product_image)}}"
                                               href="#"><img style="height: 300px"
                                                             src="{{asset('product_image/'.$image->product_image)}}"
                                                             alt="" class="simpleLens-big-image"></a>
                                        </div>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach

                                </div>
                                <div class="quick-thumb">
                                    <div class="nav nav-tabs">
                                        <ul id="tabs-details" class="owl-carousel product-slider owl-theme">
                                            @foreach($images as $image)
                                                <li class="tablist"><a data-toggle="tab"
                                                                       href="#{{$image->product_image_id}}"> <img
                                                                style="height: 100px"
                                                                src="{{asset('product_image/'.$image->product_image)}}"
                                                                alt="quick view"/> </a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7">
                            <div class="quick-right">
                                <div class="quick-right-text">
                                    <h3><strong>{{$productInfo->product_name}}</strong></h3>
                                    <div class="amount">
                                        <h4>
                                            &#2547; @if($productInfo->discount > 0){{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100}}@else{{$productInfo->product_price}}@endif</h4>
                                    </div>
                                    <p>
                                        @php
                                            print_r($productInfo->description);
                                        @endphp
                                    </p>
                                    <div class="row m-p-b">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="por-dse">
                                                <ul>
                                                    <li>
                                                        <span>Availability</span><strong>:</strong> @if($productInfo->product_quantity > 0)
                                                            In stock @else Stock Out @endif</li>
                                                    <li><span>Condition</span><strong>:</strong> New product</li>
                                                    <li><span>Category</span><strong>:</strong> <a
                                                                href="#">{{$ct->category_name}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">

                                        </div>
                                    </div>

                                    {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!}

                                    <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                    <input type="hidden" name="product_name" value="{{$productInfo->product_name}}">
                                    <input type="hidden" name="product_name_bn"
                                           value="{{$productInfo->product_name_bn}}">
                                    <input type="hidden" name="product_code" value="{{$productInfo->product_code}}">
                                    <input type="hidden" name="product_price"
                                           value="@if($productInfo->discount > 0){{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100}}@else{{$productInfo->product_price}}@endif">
                                    {{--<input type="hidden" name="product_quantity" value="1">--}}
                                    <input type="hidden" name="publication_status"
                                           value="{{$productInfo->publication_status}}">

                                    <div class="dse-btn">
                                        <div class="row">
                                            <div class="col-sm-7 col-md-6">
                                                <div class="por-dse">
                                                    <ul>

                                                        <li class="share-btn qty clearfix"><span>quantity</span>
                                                            <form action="#" method="POST">
                                                                <div class="plus-minus">
                                                                    <a class="dec qtybutton">-</a>
                                                                    <input type="text" value="1"
                                                                           name="product_quantity" class="plus-minus">
                                                                    <a class="inc qtybutton">+</a>
                                                                </div>
                                                            </form>
                                                        </li>
                                                        <li class="share-btn clearfix"><span>Size</span>
                                                            <?php
                                                            $sizes = DB::table('product_sizes')->where('product_id', $productInfo->id)->get();
                                                            ?>
                                                            <div class="">
                                                                <select name="size" id="" style="width: 100px;">
                                                                    <option value="0">Select One</option>
                                                                    @foreach($sizes as $sz)
                                                                        <option value="{{$sz->size}}">{{$sz->size}}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>

                                                        </li>


                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-md-6">
                                                <div class="por-dse clearfix responsive-othre">
                                                    <ul class="other-btn">
                                                        <li>
                                                            <div class=" add-to">
                                                                {{--<a style="width: 100px" href="#">add to cart</a>--}}
                                                                <input class="" style="width: 150px; height: 33px "
                                                                       type="submit" value="Add to Cart">
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </li>
                                                        <li>
                                                            {{--                                                            @if($wishlist==NULL)--}}
                                                            {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                                            <input type="hidden" name="session_id"
                                                                   value="{{Session::getId()}}">
                                                            <input type="hidden" name="product_id"
                                                                   value="{{$productInfo->id}}">

                                                            <button class="" style="width: 50px; height: 33px"
                                                                    type="submit"><i class="fa fa-heart"></i></button>
                                                            {!! Form::close() !!}
                                                            {{--@endif--}}
                                                        </li>
                                                        <li></li>

                                                        {{--<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>--}}

                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single-product item end -->
            </div>

        </div>
    </div>
    <!-- product-details section end -->

    <!-- new-products section start -->
    <section class="new-products single-products section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title">
                        <h3>related products</h3>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div id="new-products" class="owl-carousel product-slider owl-theme">
                    @foreach($related as $rel)
                        @php
                            $image = DB::table('product_images')->where('product_id',$rel->id)->orderBy('product_image_id','desc')->first();

                        @endphp
                    <div class="col-xs-12">
                        <div class="single-product">
                            <div class="product-img">
                                <div class="pro-type">

                                </div>
                                <a href="{{url('product-details/'.$rel->id)}}"><img class="new_arrival_height" src="{{asset('product_image/'.$image->product_image)}}"
                                                 alt="Product Title"/></a>
                            </div>
                            <div class="product-dsc">
                                <p><a href="#">{{$rel->product_name}}</a></p>
                                <span>&#2547;

                                    @if($rel->discount > 0){{ $rel->product_price-($rel->product_price*$rel->discount)/100}}@else{{$rel->product_price}}@endif</span>
                            </div>
                            <div class="actions-btn">
                                {{--<a href="#" data-toggle="modal" data-trigger="hover" data-target="#quick-view"--}}
                                   {{--data-placement="right" title="Quick View"><i class="pe-7s-expand1"></i></a>--}}
                                {{--<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i--}}
                                            {{--class="pe-7s-like"></i></a>--}}
                                {{--<a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i--}}
                                            {{--class="pe-7s-repeat"></i></a>--}}
                                {{--<a href="#" data-toggle="tooltip" data-placement="right" title="Add To Cart"><i--}}
                                            {{--class="pe-7s-cart"></i></a>--}}
                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>
    <!-- new-products section end -->
    <!--START QUICK VIEW MODAL-->

    {{--@include('includes.quick_view')--}}



@endsection