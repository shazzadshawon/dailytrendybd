<section class="testimonials section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>REVIEWS</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-text-center">
                <div id="testimonials" class="owl-carousel product-slider owl-theme">
                    <div class="single-testimonial">
                        <div class="testimonial-img">
                            <a href="#"><img src="img/testimonial/1.png" alt="Title" /></a>
                        </div>
                        <div class="testimonial-dsc">
                            <h4><a href="#">Client Name</a></h4>
                            <span>20, Oct 2017</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                    <!-- single testimonial item end -->
                    <div class="single-testimonial">
                        <div class="testimonial-img">
                            <a href="#"><img src="img/testimonial/2.png" alt="Title" /></a>
                        </div>
                        <div class="testimonial-dsc">
                            <h4><a href="#">Client Name</a></h4>
                            <span>20, Oct 2017</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                    <!-- single testimonial item end -->
                    <div class="single-testimonial">
                        <div class="testimonial-img">
                            <a href="#"><img src="img/testimonial/3.png" alt="Title" /></a>
                        </div>
                        <div class="testimonial-dsc">
                            <h4><a href="#">Client Name</a></h4>
                            <span>20, Oct 2017</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                    <!-- single testimonial item end -->
                </div>
            </div>
        </div>
    </div>
</section>