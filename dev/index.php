<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>Daily Treandy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
        <?php require('header.php'); ?>
        <!-- header section end -->
        <!-- slider-section-start -->
        <?php require('slider_main.php'); ?>
		<!-- slider section end -->
        <!-- offer section start -->
        <?php require('offer.php'); ?>
		<!-- offer section end -->
        <!-- new_arrival section start -->
        <?php require('new_arrival.php'); ?>
		<!-- new_arrival section end -->
		<!-- mid_adveretise section end -->
        <?php require('mid_adveretise.php'); ?>
        <!-- mid_adveretise section start -->
        <!-- featured_product section start -->
        <?php require('featured_product.php'); ?>
		<!-- featured_product section end -->
        <!-- review section start -->
        <?php require('review.php'); ?>
		<!-- review section end -->

        <!--START QUICK VIEW MODAL-->
        <?php require('quick_view.php'); ?>
        <!--END QUICK VIEW MODAL-->

        <!-- footer section start -->
        <?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<!-- jquery latest version -->

        <?php require('tail.php');?>
        <!-- countdown JS -->
        <script src="js/countdown.js"></script>

    </body>
</html>
