<section class="summer summer-one section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                @if(!empty($hotdeal))
                <div class="left-banner" style="background-color: #c5c5c5; background-size: 100%; background-image: url({{asset('public/uploads/hotdeal/'.$hotdeal->hotdealimage)}})">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-10 col-md-offset-5 col-md-7">
                            <div class="count-text clearfix">
                                <ul id="countdown-2">
                                    <li id="countdown_back" style="display: none"><?php echo $hotdeal->end_date;?></li>
                                    <li>
                                        <span class="days timenumbers"></span>
                                        <p class="timeRefDays timedescription">days</p>
                                    </li>
                                    <li>
                                        <span class="hours timenumbers"></span>
                                        <p class="timeRefHours timedescription">hrs</p>
                                    </li>
                                    <li>
                                        <span class="minutes timenumbers"></span>
                                        <p class="timeRefMinutes timedescription">mins</p>
                                    </li>
                                    <li>
                                        <span class="seconds timenumbers"></span>
                                        <p class="timeRefSeconds timedescription">secs</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="bannertext">
                                <h4><a href="#">{{$hotdeal->discount}} % Off</a></h4>

                                <p><?php print_r($hotdeal->description);?></p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-sm-5">
                @if(!empty($mega_offer))
                <div class="right-banner" style="background-size: 100%; background-image: url({{asset('pazzle_image/'.$mega_offer->pazzle_image)}})">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-4">

                        </div>
                        {{--<div class="col-xs-10 col-sm-10 col-md-8 text-right">--}}
                            {{--<div class="bannertext">--}}
                                {{--<h4><a href="#"></a></h4>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consecte adipisicing elit, sed do eiusmod tempor incididunt ut enim.</p>--}}
                                {{--<a href="">Shop now </a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>