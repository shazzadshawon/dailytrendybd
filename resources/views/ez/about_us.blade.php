@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif --}}

<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{url('/')}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">About Us</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Infomations</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li class="active"><span></span><a href="{{ url('about-us') }}">About Us</a></li>
                                    <li><span></span><a href="{{ url('delivery_policy') }}">Delivery Information</a></li>
                                    <li><span></span><a href="{{ url('contact-us') }}">Contact Us</a></li>                                   
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">About Us</span>
                </h2>
                <!-- Content page -->
                <div class="content-text clearfix">

                    <img src="assets/images/LP_logo_big.png" class="alignleft" width="310" alt="">

                    <p>Effortless Shopping On Lakes Point. Lakes Point.com is the largest Online Shop and becoming a leading online shop brand of Bangladesh. Here customer can find all type of essential, high-valued, quality, exclusive products and 100% original product in one website. We will deliver our products within 48 hours all over the country. In tune with the vision of Digital Bangladesh Lakes Poin.com opens the doorway for everybody to shop over the Internet. We constantly update with lot of new products, services and special offers. We provide on-time delivery of any product.</p>

                    <p>Lakes Point is an online shopping destination where you can shop the widest selection of electronics, fashion,Health,Fitness& Sports, home appliances, kid’s items and more in Bangladesh and have them shipped directly to your home or office at your convenience!</p>

                    <p>We offer free returns and various payment options including cash on delivery. With affordable prices and great products, Daraz lets you enjoy an awesome shopping experience with your order sent directly to your doorstep. No Bay , no Gain, super convenience guaranteed.</p>

                    <p>We are constantly expanding our product range to include the latest gadgets, fashion styles and new categories and we don't want you to miss out! Follow us on Facebook Instragram ,Whats App Group and Twitter to stay updated on the latest offers and Lakes Point news. Happy shopping.</p>
                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<!-- ./page wapper-->
<!-- Footer -->
@endsection