<footer class="section-padding-top">
    <!-- service section start -->
    <div class="service section-padding">
        <div class="container">
            <div class="row text-center">
                <div class="col-xs-12 col-sm-4">
                    <div class="single-service">
                        <i class="pe-7s-plane"></i>
                        <h4>Free Shipping</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="single-service">
                        <i class="pe-7s-headphones"></i>
                        <h4>Customer Support</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="single-service">
                        <i class="pe-7s-refresh"></i>
                        <h4>15 days money back</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service section end -->
    <!-- footer-top area start -->
    <div class="footer-top section-padding-top">
        <div class="footer-dsc">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="single-text">
                            <div class="footer-title">
                                <h4>Contact us</h4>
                                <hr class="dubble-border"/>
                            </div>
                            <div class="footer-text">
                                <ul>
                                    <li>
                                        <i class="pe-7s-map-marker"></i>
                                        <p>Mirpur 10, Dhaka</p>
                                        <p>Bangladesh 1216</p>
                                    </li>
                                    <li>
                                        <i class="pe-7s-call"></i>
                                        <p>+880 1741228971</p>
                                        <p>+880 1741228971</p>
                                    </li>
                                    <li>
                                        <i class="pe-7s-mail-open"></i>
                                        <p>info@dailytrendybd.com</p>
                                        <p>contact@dailytrendybd.com</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="single-text">
                            <div class="footer-title">
                                <h4>my account</h4>
                                <hr class="dubble-border"/>
                            </div>
                            <div class="footer-menu">
                                <ul>
                                    <li><a href="{{url('cart')}}">Shopping Cart</a></li>
                                    <li><a href="{{url('wishlist')}}">My Wishlist</a></li>
                                    <li><a href="{{url('shipping')}}">Chechout</a></li>
                                    <li><a href="{{url('about')}}">About Us</a></li>
                                    <li><a href="{{url('contact')}}">Contact Us</a></li>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 margin-top">
                        <div class="single-text">
                            <div class="footer-title">
                                <h4>NEWSLETTER</h4>
                                <hr class="dubble-border"/>
                            </div>
                            <div class="footer-text">
                                <p>Lorem ipsum dolor sit amet, consectetures do adipisicing elit, sed do eiusmod tempores incididunt ut labore</p>
                                <form role="form" method="post" action="{{ url('subscribe') }}">
                                    {{ csrf_field() }}
                                    <input type="email" name="email" placeholder="Enter your mail" />
                                    <input type="submit" value="submit" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="dubble-border"/>
        </div>
        <div class="social-media">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">

                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="social-icon">
                            <ul class="floatright">
                                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-soundcloud"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-top area end -->
    <!-- footer-bottom area start -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>Created with <i class="fa fa-heart" style="color:red" aria-hidden="true"></i> <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom area end -->
</footer>