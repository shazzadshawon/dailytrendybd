@extends('layouts.frontend')

@section('content')
<!-- HEADER -->
<div class="pages-title section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="pages-title-text">
                    <h3>Login</h3>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><span>/</span>Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end header -->


<section class="pages login-page section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="login-text">
                    <div class="log-title">
                        <h3><strong>registered customers</strong></h3>
                        <hr />
                    </div>
                    <div class="custom-input">
                        <p>If you have an account with us, Please log in!</p>
                        {!! Form::open(['url' => '/customer-login-check', 'method'=>'POST']) !!}
                            <input type="text" name="email_address" placeholder="Email" />
                            <input type="password" name="password" placeholder="Password" />

                            <div class="submit-text coupon">
                                <input type="submit" name="" value="Login" />
                            </div>
                        {{  Form::close()  }}
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="login-text new-customer">
                    <div class="log-title">
                        <h3><strong>new customers</strong></h3>
                        <hr />
                    </div>
                    <div class="custom-input">
                        {!! Form::open(['route' => 'customer.store']) !!}
                            <input type="text" name="customer_name" placeholder="Name" required/>
                            <input type="email" name="email_address" placeholder="Email Address" required/>
                            <input type="text" name="phone_number" placeholder="Phone Number" required/>

                            <input type="text" name="address" placeholder="Address" required/>

                            <input type="password" name="password" placeholder=" Password" required/>
                            <input type="password" name="confirm_password" placeholder="Confirm Password" required/>


                            <div class="submit-text coupon">
                                <input type="submit" name="" value="Register" />
                            </div>
                        {{  Form::close()  }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection