<!doctype html>
<html class="no-js" lang="">
    <head>
        <title>About Us || Daily Treandy BD</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- header section start -->
		<?php require('header.php'); ?>
        <!-- header section end -->
        <!-- pages-title-start -->
		<div class="pages-title about-page section-padding">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<div class="pages-title-text">
							<h3>About Us</h3>
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><span>/</span>About Us</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- pages-title-end -->
        <!-- about-us-section-start -->
		<div class="about-us section-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="about-img">
							<img src="img/about/l.jpg" alt="" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="about-text">
							<div class="about-author">
								<h3>We are crazy fashion</h3>
								<hr />
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempores incidi dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercing tation ullamco laboris nisi ut aliquip ex ea commodo consequats. Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt culp qui offici deserunt mollit anim id est laborum.</p>
							<ul>
								<li><i>Who you are</i></li>
								<li><i>Why you sell the items you sell</i></li>
								<li><i>Where you are located</i></li>
								<li><i>How long you have been in business</i></li>
							</ul>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dalese orem que laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veri tatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- about-us section end -->
        <!-- team-member section start -->
		<section class="team-member single-products">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="section-title">
							<h3>our creative team</h3>
						</div>
					</div>
				</div>
				<div class="row text-center">
					<div id="new-products" class="owl-carousel product-slider owl-theme">
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/about/1.jpg" alt="Team Member" /></a>
								</div>
								<div class="member-title">
									<h4>Jeffrey Ortega</h4>
									<p>Fashion Designer</p>
								</div>
								<div class="actions-btn">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
						<!-- single member end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/about/2.jpg" alt="Team Member" /></a>
								</div>
								<div class="member-title">
									<h4>Jeffrey Ortega</h4>
									<p>Fashion Designer</p>
								</div>
								<div class="actions-btn">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
						<!-- single member end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/about/3.jpg" alt="Team Member" /></a>
								</div>
								<div class="member-title">
									<h4>Jeffrey Ortega</h4>
									<p>Fashion Designer</p>
								</div>
								<div class="actions-btn">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
						<!-- single member end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/about/4.jpg" alt="Team Member" /></a>
								</div>
								<div class="member-title">
									<h4>Jeffrey Ortega</h4>
									<p>Fashion Designer</p>
								</div>
								<div class="actions-btn">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
						<!-- single member end -->
						<div class="col-xs-12">
							<div class="single-product">
								<div class="product-img">
									<a href="#"><img src="img/about/2.jpg" alt="Team Member" /></a>
								</div>
								<div class="member-title">
									<h4>Jeffrey Ortega</h4>
									<p>Fashion Designer</p>
								</div>
								<div class="actions-btn">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
						<!-- single member end -->
					</div>
				</div>
			</div>
		</section>
		<!-- team-member section end -->
        <!-- footer section start -->
		<?php require('footer.php'); ?>
        <!-- footer section end -->
        
		<!-- all js here -->
		<?php require('tail.php'); ?>
    </body>
</html>
